document.addEventListener("deviceready", onDeviceReady, false);

var currentRow;
// Populate the database
//
function populateDB(tx) {
    tx.executeSql('CREATE TABLE IF NOT EXISTS NERU_USUARIO (id INTEGER PRIMARY KEY AUTOINCREMENT, nombre, usuario, stado_pago, grupo, stado_sesion)');
}

// Query the database
//
function queryDB(tx) {
    tx.executeSql('SELECT * FROM NERU_USUARIO', [], querySuccess, errorCB);
}

function searchQueryDB(tx) {
    tx.executeSql("SELECT * FROM NERU_USUARIO where usuario like ('%"+ document.getElementById("txtName").value + "%')",
            [], querySuccess, errorCB);
}
// Query the success callback
//
function querySuccess(tx, results) {
    var stado = 0;
    for (var i = 0; i < len; i++) {
        var perfil=results.rows.item(i).stado_pago;
        if(perfil == 1){
            stado = 1;
        }else{
            stado = 0;
        }
    }
    if(stado != 0){
        $.mobile.changePage('pages/home.html', {});
    }else{
        $.mobile.changePage('presentacion.html', {});
    }
}

//Delete query
function deleteRow(tx) {
  tx.executeSql('DELETE FROM NERU_USUARIO WHERE id = ' + currentRow, [], queryDB, errorCB);
}

// Transaction error callback
//
function errorCB(err) {
    alert("Error processing SQL: "+err.code);
}

// Transaction success callback
//
function successCB() {
    var db = window.openDatabase("Database", "1.0", "Cordova NERU_USUARIO", 200000);
    db.transaction(queryDB, errorCB);
}

 // Cordova is ready
//
function onDeviceReady() {
    var db = window.openDatabase("Database", "1.0", "Cordova NERU_USUARIO", 200000);
    db.transaction(populateDB, errorCB, successCB);
}

//Insert query
//
function insertDB(tx) {
    tx.executeSql('INSERT INTO NERU_USUARIO (nombre,usuario,stado_pago,grupo,stado_sesion) VALUES ("' +document.getElementById("txtName").value
            +'","'+document.getElementById("txtNumber").value+'")');
}

function goInsert() {
    var db = window.openDatabase("Database", "1.0", "Cordova NERU_USUARIO", 200000);
    db.transaction(insertDB, errorCB, successCB);
}

function goSearch() {
    var db = window.openDatabase("Database", "1.0", "Cordova NERU_USUARIO", 200000);
    db.transaction(searchQueryDB, errorCB);
}

function goDelete() {
     var db = window.openDatabase("Database", "1.0", "Cordova NERU_USUARIO", 200000);
     db.transaction(deleteRow, errorCB);
     document.getElementById('qrpopup').style.display='none';
}

//Show the popup after tapping a row in table
//
function goPopup(row,rowname,rownum) {
    currentRow=row;
    document.getElementById("qrpopup").style.display="block";
    document.getElementById("editNameBox").value = rowname;
    document.getElementById("editNumberBox").value = rownum;
}

function editRow(tx) {
    tx.executeSql('UPDATE NERU_USUARIO SET name ="'+document.getElementById("editNameBox").value+
            '", number= "'+document.getElementById("editNumberBox").value+ '" WHERE id = '
            + currentRow, [], queryDB, errorCB);
}
function goEdit() {
    var db = window.openDatabase("Database", "1.0", "Cordova NERU_USUARIO", 200000);
    db.transaction(editRow, errorCB);
    document.getElementById('qrpopup').style.display='none';
}