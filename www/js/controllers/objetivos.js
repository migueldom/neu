$(document).on('pagebeforeshow', '#page-objetivos', function (e) {

    $("#wizard").steps({
        headerTag: "h2",
        bodyTag: "section",
        transitionEffect: "slideLeft"
    });

});

function agregar_objetivos(id_usuario,obj_gen_mp,obj_gen_lp,obj_esp_fis,obj_esp_tec,obj_esp_psi,obj_lp_fis,obj_lp_tec,obj_lp_psi){
    var t3 = "guarda_objetivo," + id_usuario + ","+ obj_gen_mp +","+ obj_gen_lp +","+ obj_esp_fis +","+ obj_esp_tec +","+ obj_esp_psi +","+ obj_lp_fis +","+ obj_lp_tec +","+ obj_lp_psi +"," + new Date().getTime();
    $.ajax({
        url: 'https://www.nerupsicologia.com/app/Php/data.php',
        type: 'post',
        data: { datos: t3 },
        dataType: 'json',
        success: function (data) {
            if (data.success) {
                actualizar_evaluacion_usuario(id_usuario); 
                localStorage.setItem('objetivoG','0'); 
            } else {
                navigator.notification.alert(
                    data.msg,  // message
                    alertDismissed,         // callback
                    'Aviso',            // title
                    'Aceptar'                  // buttonName
                );
            }
        }
    });
}

function actualizar_evaluacion_usuario(idU) {
    var t3 = "update_usuario_variables," + idU + "," + new Date().getTime();
    $.ajax({
        url: 'https://www.nerupsicologia.com/app/Php/data.php',
        type: 'post',
        data: { datos: t3 },
        dataType: 'json',
        success: function (data) {
            if (data.success) {
                var pem = localStorage.getItem('var_pem');
                localStorage.setItem('var_pem','1');
                regresarPEM();    
            } else {
                navigator.notification.alert(
                    data.msg,  // message
                    alertDismissed,         // callback
                    'Aviso',            // title
                    'Aceptar'                  // buttonName
                );
            }
        }
    });
}