$(document).on('pageinit', function(){
    function alertDismissed() {
        // do something
    }
    $("#btnRecuperar").click( function(){
        var email = $("#email").val();
        var pass = $("#passwordR").val();
        var passD = $("#passwordD").val();
        console.log(pass);
        console.log(passD);
        if(pass == passD){
            var t3 = "update_pass,"+email+","+pass+","+new Date().getTime();
            /****METODO AJAX******/
            $.ajax({
                url:'https://www.nerupsicologia.com/app/Php/data.php',
                type: 'post',
                data: {datos:t3},
                dataType: 'json',
                success: function(data){
                    if(data.success){
                        navigator.notification.alert(
                            'Se ha actualizado correctamente',  // message
                            alertDismissed,         // callback
                            'Registro Completo',            // title
                            'Aceptar'                  // buttonName
                        );
                        $.mobile.changePage('../presentacion.html', {});
                    }else{
                        navigator.notification.alert(
                            data.msg,  // message
                            alertDismissed,         // callback
                            'Registro Incorrecto',            // title
                            'Aceptar'                  // buttonName
                        );
                    }
                }
            });
            /**FIN AJAX****** */
        }else{
            navigator.notification.alert(
                'Contraseñas no coinciden/ minimo de 8 caracteres, revisa por favor',  // message
                alertDismissed, // callback
                'Problemas',    // title
                'Aceptar'       // buttonName
            );
            $("#password").focus();
        }
        
    });
});