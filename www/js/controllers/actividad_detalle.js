function actividad_detalle(){
    $.mobile.changePage('pages/page-pem_actividad_detalle.html', {});
}

$(document).on('pagebeforeshow', '#page-pem_actividad', function (e) {
    var idActividad = localStorage.getItem('actividad_id');
    var nAct = parseInt(idActividad) - 1;
    var idusuario = localStorage.getItem('id_usuario');
    var idVariable = localStorage.getItem("variable_id");
    var t3 = "actividad_instruccion,"+idActividad+","+idusuario+","+idVariable+","+new Date().getTime();
        /**AJAX**/
        $.ajax({
            url:'https://www.nerupsicologia.com/app/Php/data.php',
            type: 'post',
            data: {datos:t3},
            dataType: 'json',
            success: function(data){
                if(data.success){
                    $.each(data, function(index, record){
                        if($.isNumeric(index)){
                            var multimedia = "";
                            if(record.audio != ""){
                                multimedia = "<iframe src='"+record.audio+"' frameborder='2'></iframe>";
                            }else{
                                multimedia = "";
                            }
                            var ind = parseInt(index) + 1;
                            $("#actividad").append("<section><p style='color:black'>"+record.descripcion+"</p>"+
                            "<input type='hidden' id='nacti"+index+"' value="+index+" /></section>");
                            if(multimedia.length == 0){
                                $("#multimedia").css('display','none');
                            }else{
                                $("#multimedia").append("<div>"+multimedia+"</div>");
                            }
                            
                            $("#numero_actividad").append("<div>"+record.id_instrucciones+"</div>");
                        }
                    });
                }else{
                    navigator.notification.alert(
                        data.msg,  // message
                        alertDismissed,         // callback
                        'Aviso',            // title
                        'Aceptar'                  // buttonName
                    );
                    setTimeout(function(){ 
                        $.mobile.changePage('pages/page-pem_info_concentra.html', {});
                    }, 1000);
                }
                
            }
        });
        /**FIN_AJAX**/
});

/*DETALLE*/
$(document).on('pagebeforeshow', '#page-pem_actividad_detalle', function (e) {
    
});
function alertDismissed() {}

function consultaActi(numAct){
    numAct = numAct + 1;
    $("#titulo_actividad").text(numAct);
}

function regresar_actividades(){
    $.mobile.changePage('pages/page-pem_actividad.html', {});
}