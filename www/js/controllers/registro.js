$(document).on('pageinit', function(){

    function alertDismissed() {
        // do something
    }

    $("#btnCrear").click( function(){
        var nombre = $("#nombre").val();
        var app = $("#app").val();
        var apm = $("#apm").val();
        var fec_nac = $("#fec_nac").val();
        var email = $("#email").val();
        var usuarior = $("#usuarior").val();
        var password = $("#password").val();

        cargarLoad();
        if(usuarior != "" || usuarior.length > 3 || email.length != ""){
            var t3 = "add,"+nombre+","+app+","+apm+","+usuarior+","+password+","+email+","+fec_nac+","+new Date().getTime();
            $.ajax({
                url: 'https://www.nerupsicologia.com/app/Php/data.php',
                type: 'post',
                data: {datos:t3},
                dataType: 'json',
                success: function(data){
                    cordova.plugin.pDialog.dismiss();
                    if(data.success){
                        navigator.notification.alert(
                            'Se ha registrado correctamente',  // message
                            alertDismissed,         // callback
                            'Registro Completo',            // title
                            'Aceptar'                  // buttonName
                        );
                        $.mobile.changePage('../presentacion.html', {});
                    }
                }
            });
        }else{
            navigator.notification.alert(
                'Revise el usuario no sea vacio o mayor de 3 caracteres/ Debes proporcionar correo electrónico',  // message
                alertDismissed,         // callback
                'AVISO',            // title
                'Aceptar'                  // buttonName
            );
        }
        
    });
});