$(document).on('pagebeforeshow', '#page-home', function (e) {
    cordova.plugins.notification.local.schedule({
        title: 'Regresa pronto',
        text: 'No olvides realizar tu actividad del día',
        foreground: true,
        vibrate:true,
        trigger: { every: 'day', hour: 8 }
    });
    cordova.plugins.notification.local.schedule({
        title: 'Regresa pronto',
        text: 'No olvides entrenar tu mente para jugar mejor',
        foreground: true,
        vibrate:true,
        trigger: { every: 'day', hour: 15 }
    });
    cordova.plugins.notification.local.schedule({
        title: 'Regresa pronto',
        text: 'Si Controlas tu mente controlas el Juego',
        foreground: true,
        vibrate:true,
        trigger: { every: 'day', hour: 19 }
    });

    comprobarSesion();
    cordova.plugin.pDialog.dismiss();
    //Programa de entrenamiento Click
    $('.flexslider').flexslider();
    //Realidad Virtual Click
    $("#playsic_rv-menu").click(function () {
        $.mobile.changePage('pages/page-playsic_rv.html', {});
    });
    //PSIC ONLINE Click
    $("#psicologia_online-menu").click(function () {
        $.mobile.changePage('pages/page-psicologia.html', {});
    });
    //Cursos Click
    $("#cursos-menu").click(function () {
        $.mobile.changePage('pages/page-cursos.html', {});
    });
    //Perfil Click
    $("#perfil-menu").click(function () {
        $.mobile.changePage('pages/page-perfil.html', {});
    });
    //Noticias Click
    $("#noticias-menu").click(function () {
        $.mobile.changePage('pages/page-noticias.html', {});
    });
    
});
function alertDismissed() {
    cordova.plugin.pDialog.dismiss();
    $.mobile.changePage('presentacion.html', {});
}
function cargarLoad(){
    cordova.plugin.pDialog.init({
        theme : 'HOLO_DARK',
        progressStyle : 'SPINNER',
        cancelable : false,
        title : 'Espere por favor...',
        message : 'Cargando ...'
    });
}
function comprobarSesion(){
    cargarLoad();
    var usuario = localStorage.getItem("id_usuario");
    var t3 = "consulta_sesion,"+usuario+","+new Date().getTime();
    /**AJAX**/
    $.ajax({
        url: 'https://www.nerupsicologia.com/app/Php/data.php',
        type: 'post',
        data: { datos: t3 },
        dataType: 'json',
        success: function (data) {
            if (data.success) {
                console.log(data.pago);
                cordova.plugin.pDialog.dismiss();
                localStorage.setItem("pago", data.pago);
            } else {
                localStorage.clear();
                navigator.notification.alert(
                    data.msg,  // message
                    alertDismissed,         // callback
                    'Aviso',            // title
                    'Aceptar'                  // buttonName
                );
                //navigator.app.exitApp();
            }
        }
    });
    /**FIN_AJAX**/
    $("#wizardInfo").steps({
        headerTag: "h2",
        bodyTag: "section",
        transitionEffect: "slideLeft"
    });
}

function soloNumeros(e){
    var key = window.event ? e.which : e.keyCode;
    if (key < 48 || key > 57) {
        e.preventDefault();
    }
}