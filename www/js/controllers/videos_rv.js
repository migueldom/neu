$(document).on('pagebeforeshow', '#page-realidad_v', function (e) {
    $("#wizardInfoRV").steps({
        headerTag: "h2",
        bodyTag: "section",
        transitionEffect: "slideLeft"
    });
    var t3 = "consulta_vr," + new Date().getTime();
    /**AJAX**/
    $.ajax({
        url: 'https://www.nerupsicologia.com/app/Php/data.php',
        type: 'post',
        data: { datos: t3 },
        dataType: 'json',
        success: function (data) {
            if (data.success) {
                $.each(data, function (index, record) {
                    if ($.isNumeric(index)) {
                        var video = record.url_video;
                        var multimedia = "";
                        if(record.url_video != ""){
                            console.log(video);
                            multimedia = "<button onclick=openTube('"+video+"')>DEMO TÉCNICA DE RESPIRACIÓN</button>";
                        }else{
                            multimedia = "";
                        }
                        if(multimedia.length == 0){
                            $(".div_videos_slide").css('display','none');
                        }else{
                            $(".div_videos_slide").append("<div>"+multimedia+"</div>");
                        }
                    }
                });
            } else {
                navigator.notification.alert(
                    data.msg,  // message
                    alertDismissed,         // callback
                    'Aviso',            // title
                    'Aceptar'                  // buttonName
                );
            }
        }
    });
    /**FIN_AJAX**/
});

function openTube(c){
    try {
        window.InAppYouTube.openVideo(c, {
          fullscreen: true
        }, function(result) {
          // console.log(JSON.stringify(result));
        }, function(reason) {
          // console.log(reason);
        });
      } catch(e) {
        // Exception!
      }
}

function infoRendimiento(x){
    if(x == 1){
        $.mobile.changePage('pages/page_info_rendimiento.html', {});
    }else if(x == 2){
        $.mobile.changePage('pages/page_info_rendimientoD.html', {});
    }else if(x == 4){
        $.mobile.changePage('pages/page_info_grafica.html', {});
    }else{
        $.mobile.changePage('pages/page_info_grafica.html', {});
    }
}