$(document).on('pageinit', function(){
    (function(){function $MPC_load(){window.$MPC_loaded !== true && (function(){var s = document.createElement("script");s.type = "text/javascript";s.async = true;s.src = document.location.protocol+"//secure.mlstatic.com/mptools/render.js";var x = document.getElementsByTagName('script')[0];x.parentNode.insertBefore(s, x);window.$MPC_loaded = true;})();}window.$MPC_loaded !== true ? (window.attachEvent ?window.attachEvent('onload', $MPC_load) : window.addEventListener('load', $MPC_load, false)) : null;})();
});


$(document).on('pagebeforeshow', '#page-grupo', function (e) {
    $(".div_grupo").css('visibility','hidden');
    revisaEq();
    var bannerImageEq = document.getElementById('bannerImgEq');
    var resultEq = document.getElementById('resEq');
    var imgEq = document.getElementById('imgPerfilEq');

// 
bannerImageEq.addEventListener('change', function() {
    var file = this.files[0];
    // declare a maxSize (3Mb)
    var maxSize = 3000000;
    $("#imgPerfilEq").css('display','block');
    if (file.type.indexOf('image') < 0) {
        resEq.innerHTML = 'invalid type';
        return;
    }
    var fReader = new FileReader();
    fReader.onload = function() {
    	imgEq.onload = function(){
    		// if localStorage fails, it should throw an exception
			try{
				// pass the ratio of the file size/maxSize to your toB64 func in case we're already out of scope
				localStorage.setItem("imgDataEq", getBase64Image(imgEq, (file.size/maxSize), file.type));
        		}
			catch(e){
				var msg = e.message.toLowerCase();
				// We exceeded the localStorage quota
				if(msg.indexOf('storage')>-1 || msg.indexOf('quota')>-1){
					// we're dealing with a jpeg image :  try to reduce the quality
					if(file.type.match(/jpe?g/)){
						localStorage.setItem("imgDataEq", getBase64Image(imgEq, (file.size/maxSize), file.type, 0.7));
						}
					// we're dealing with a png image :  try to reduce the size
					else{
						// maxSize is a total approximation I got from some tests with a random pixel generated img
						var maxPxSize = 750000,
						imgSize = (img.width*img.height);
						localStorage.setItem("imgDataEq", getBase64Image(imgEq, (imgSize/maxPxSize), file.type));
						}
					}
				}
			}
        imgEq.src = fReader.result;
    };
    
    fReader.readAsDataURL(file);
});

function getBase64Image(img, sizeRatio, type, quality) {
	// if we've got an svg, don't convert it, svg will certainly be less big than any pixel image
	if(type.indexOf('svg+xml')>0) return img.src;

	// if we've got a jpeg
	if(type.match(/jpe?g/)){
		// and the sizeRatio is okay, don't convert it
		if(sizeRatio<=1) return img.src;
		}
	// if we've got some other image type
	else type = 'image/png';

	if(!quality) quality = 1;
    var canvas = document.createElement("canvas");
	// if our image file is too large, then reduce its size
    canvas.width = (sizeRatio>1)?(img.width/sizeRatio): img.width;
    canvas.height = (sizeRatio>1)?(img.height/sizeRatio): img.height;

    var ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
    // if we already tried to reduce its size but it's still failing, then reduce the jpeg quality
    var dataURL = canvas.toDataURL(type, quality);
    
    return dataURL;
}

function fetchimage () {
    var dataImage = localStorage.getItem('imgDataEq');
    imgEq.src = dataImage;
}

// Call fetch to get image from localStorage.
fetchimage();
});    

function guardaEquipo(){
    var imgEq = localStorage.getItem('imgDataEq');
    var nombreEquipo = $("#tituloEquipo").val();
    var nombreDT = $("#nombreDT").val();
    var liga = $("#liga").val();
    var numeroJugadores = $("#numero_jugadores").val();
    var usuario = localStorage.getItem("id_usuario");
    var t3 = "guarda_equipo,"+usuario+","+nombreEquipo+","+nombreDT+","+liga+","+numeroJugadores+","+imgEq+","+new Date().getTime();
    cordova.plugin.pDialog.init({
        theme : 'HOLO_DARK',
        progressStyle : 'SPINNER',
        cancelable : false,
        title : 'Espere por favor...',
        message : 'Cargando ...'
    });
    /**AJAX**/
    $.ajax({
        url: 'https://www.nerupsicologia.com/app/Php/data.php',
        type: 'post',
        data: { datos: t3 },
        dataType: 'json',
        success: function (data) {
            if (data.success) {
                $("#codigoEq").text(data.msg);
                cordova.plugin.pDialog.dismiss();
            } else {
                navigator.notification.alert(
                    data.msg,  // message
                    alertDismissed,         // callback
                    'Aviso',            // title
                    'Aceptar'                  // buttonName
                );
            }
        }
    });
    /**FIN_AJAX**/
}
function revisaEq(){
    var usuario = localStorage.getItem("id_usuario");
    var t3 = "consulta_equipo,"+usuario+","+new Date().getTime();
    cordova.plugin.pDialog.init({
        theme : 'HOLO_DARK',
        progressStyle : 'SPINNER',
        cancelable : false,
        title : 'Espere por favor...',
        message : 'Cargando ...'
    });
    /**AJAX**/
    $.ajax({
        url: 'https://www.nerupsicologia.com/app/Php/data.php',
        type: 'post',
        data: { datos: t3 },
        dataType: 'json',
        success: function (data) {
            cordova.plugin.pDialog.dismiss();
            if (data.success) {
                $("#pg_gpo_content").css('visibility','visible');
                $.mobile.changePage('pages/page-grupo_info.html', {});
            } else {
                navigator.notification.alert(
                    "No tienes algún equipo asociado",  // message
                    alertDismissedd,         // callback
                    'Aviso',            // title
                    'Aceptar'                  // buttonName
                );
            }
        }
    });
    /**FIN_AJAX**/
    setTimeout(function(){
        $("#pasos").steps({
            headerTag: "h2",
            bodyTag: "section",
            transitionEffect: "slideLeft"
        });
        $(".div_grupo").css('visibility','visible');
    },1000);
}
function alertDismissedd(){}

$(document).on('pagebeforeshow', '#page-pago', function (e) {
    setTimeout(function(){
        cordova.plugin.pDialog.dismiss();
    },2000);
    $("#divPagos").css('display','block');
});

$(document).on('pagebeforeshow', '#page-grupo_info', function (e) {
        var usuario = localStorage.getItem("id_usuario");
        var t3 = "consulta_equipo,"+usuario+","+new Date().getTime();
        cordova.plugin.pDialog.init({
            theme : 'HOLO_DARK',
            progressStyle : 'SPINNER',
            cancelable : false,
            title : 'Espere por favor...',
            message : 'Cargando ...'
        });
        var dataImage;
        /**AJAX**/
        $.ajax({
            url: 'https://www.nerupsicologia.com/app/Php/data.php',
            type: 'post',
            data: { datos: t3 },
            dataType: 'json',
            success: function (data) {
                console.log(data);
                cordova.plugin.pDialog.dismiss();
                if (data.success) {
                    var imgEq = document.getElementById('imgGpoEq');
                    dataImage = localStorage.getItem('imgDataEq');
                    if(dataImage != null ){
                        imgEq.src = dataImage;
                    }
                    localStorage.setItem('idEquipo',data.equipo.id_equipo);
                    $("#nombre_dt_perfil").text(data.equipo.nombre_entrenador);
                    $("#liga_pertenece_perfil").text(data.equipo.liga);
                    $("#codigo_equipoPFL").text(data.equipo.codigo);
                    $("#nombre_equipo").text(data.equipo.nombre_equipo);
                    var obj = data.jugadores;
                    var s = '';
                    for(var a = 0; a < obj.length; a++){
                       s += '<li>'+obj[a].nombre_jugador+' <a onclick="ver_perfil_list('+obj[a].id_jugador+')"> <img src="./img/iconos/loupe.png" class="icono_menu" width="20" height="20"></a></li>'; 
                    }
                    $("#list_jugadores").append(s);
                }
            }
        });
        /**FIN_AJAX**/
        $("#wizardInfo").steps({
            headerTag: "h2",
            bodyTag: "section",
            transitionEffect: "slideLeft"
        });
        var pagoS = localStorage.getItem("pago");
        if(pagoS == 0){
            setTimeout(function(){ $("#popupInfo").popup("open");}, 1000);
        }
});

function ver_perfil_list(a){
    localStorage.setItem('id_jugador_actual',a);
    $.mobile.changePage('pages/page-perfil_individual.html', {});
}
$(document).on('pagebeforeshow', '#page-rendimiento_general', function (e) {
    ver_equipo_estadistica(1);
    localStorage.setItem("graficaInd",'4');
});

function ver_equipo_estadistica(a){
    var equipo = localStorage.getItem('idEquipo');
    $('#radar-chart').remove(); // this is my <canvas> element
    $('#div_graf').append('<canvas id="radar-chart" width="280" height="430"></canvas>');
    
    var t3 = "equipo_estadisticas,"+a+","+equipo+","+new Date().getTime();
        cordova.plugin.pDialog.init({
            theme : 'HOLO_DARK',
            progressStyle : 'SPINNER',
            cancelable : false,
            title : 'Espere por favor...',
            message : 'Cargando ...'
        });
        /**AJAX**/
        $.ajax({
            url: 'https://www.nerupsicologia.com/app/Php/data.php',
            type: 'post',
            data: { datos: t3 },
            dataType: 'json',
            success: function (data) {
                cordova.plugin.pDialog.dismiss();
                if (data.success) {
                    var ce = 0;
                    var ac = 0;
                    var co = 0;
                    var cl = 0;
                    var af = 0;
                    var mo = 0;
                    var ob = 0;
                    var ce2 = 0;
                    var ac2 = 0;
                    var co2 = 0;
                    var cl2 = 0;
                    var af2 = 0;
                    var mo2 = 0;
                    var ob2 = 0;
                    var ro = 0;
                    var hl = 0;
                    var ct = 0;
                    var tn = 0;
                    var promEntr = 0;
                    var promCom =0 ;
                    var promCon1 = 0;
                    var promCon2 = 0;
                    var promEst1 = 0;
                    var promEst2 = 0;
                    var promCemo1 = 0;
                    var promCemo2 = 0;
                    var promMot1 = 0;
                    var promMot2 = 0;
                    var promAct1 = 0;
                    var promAct2 = 0;
                    var promObj1 = 0;
                    var promObj2 = 0;
                    var promAuto1 = 0;
                    var promAuto2 = 0;
                    var promCfis = 0;
                    var promHaMe = 0;
                    var promCapTec = 0;
                    var promTal = 0;
                    var countVar = 0;
                    var njuga = 0;
                    $.each(data, function (index, record) {
                        if ($.isNumeric(index)) {
                            var vaid = record.id_variable;
                            var resFinal = 0;
                            var resFinal2 = 0;
                            var ces = 0;
                            var act = 0;
                            var con = 0;
                            var cem = 0;
                            var aco = 0;
                            var mot = 0;
                            var obj = 0;
                            var ces2 = 0;
                            var act2 = 0;
                            var con2 = 0;
                            var cem2 = 0;
                            var aco2 = 0;
                            var mot2 = 0;
                            var obj2 = 0;
                            var rfi = 0;
                            var hml = 0;
                            var cat = 0;
                            var tna = 0;
                            console.log(record.id_usuario+"<->"+record.id_variable+"<->"+record.resultado);
                            resFinal = record.resultado;
                            resFinal2 = record.resultado2;
                            if(resFinal=='undefined'){resFinal = 0;}
                            if(resFinal2=='undefined'){resFinal2 = 0;}
                            if (a == "1") {
                                //RENDIMIENTO GENERAL
                                switch (vaid) {
                                    case "1":
                                        con = parseInt(resFinal);
                                        con2 = parseInt(resFinal2);
                                        promEntr = parseInt(promEntr) + parseInt(resFinal);
                                        promCom = parseInt(promCom) + parseInt(resFinal2);
                                        countVar  = parseInt(countVar) + 1;
                                        break;
                                    case "2":
                                        aco = parseInt(resFinal);
                                        aco2 = parseInt(resFinal2);
                                        promEntr = parseInt(promEntr) + parseInt(resFinal);
                                        promCom = parseInt(promCom) + parseInt(resFinal2);
                                        countVar  = parseInt(countVar) + 1;
                                        break;
                                    case "3":
                                        ces = parseInt(resFinal);
                                        ces2 = parseInt(resFinal2);
                                        promEntr = parseInt(promEntr) + parseInt(resFinal);
                                        promCom = parseInt(promCom) + parseInt(resFinal2);
                                        countVar  = parseInt(countVar) + 1;
                                        break;
                                    case "4":
                                        act = parseInt(resFinal);
                                        act2 = parseInt(resFinal2);
                                        promEntr = parseInt(promEntr) + parseInt(resFinal);
                                        promCom = parseInt(promCom) + parseInt(resFinal2);
                                        countVar  = parseInt(countVar) + 1;
                                        break;
                                    case "5":
                                        mot = parseInt(resFinal);
                                        mot2 = parseInt(resFinal2);
                                        promEntr = parseInt(promEntr) + parseInt(resFinal);
                                        promCom = parseInt(promCom) + parseInt(resFinal2);
                                        countVar  = parseInt(countVar) + 1;
                                        break;    
                                    case "6":
                                        cem = parseInt(resFinal);
                                        cem2 = parseInt(resFinal2);
                                        promEntr = parseInt(promEntr) + parseInt(resFinal);
                                        promCom = parseInt(promCom) + parseInt(resFinal2);
                                        countVar  = parseInt(countVar) + 1;
                                        break;
                                    case "7":
                                        obj = parseInt(resFinal);
                                        obj2 = parseInt(resFinal2);
                                        promEntr = parseInt(promEntr) + parseInt(resFinal);
                                        promCom = parseInt(promCom) + parseInt(resFinal2);
                                        countVar  = parseInt(countVar) + 1;
                                        break;        
                                    default:
                                        con = 0;
                                        aco = 0;
                                        ces = 0;
                                        act = 0;
                                        cem = 0;
                                        mot = 0;
                                        obj = 0;
                                        con2 = 0;
                                        aco2 = 0;
                                        ces2 = 0;
                                        act2 = 0;
                                        cem2 = 0;
                                        mot2 = 0;
                                        obj2 = 0;
                                    break;
                                }  
                                co = co+con;
                                ce = ce+ces;
                                ac = ac+act;
                                af = af+aco;
                                cl = cl+cem;
                                mo = mo+mot;
                                ob = ob+obj;
                                co2 = co2+con2;
                                ce2 = ce2+ces2;
                                ac2 = ac2+act2;
                                af2 = af2+aco2;
                                cl2 = cl2+cem2;
                                mo2 = mo2+mot2;
                                ob2 = ob2+obj2;
                            }else if(a == "2"){
                                switch (vaid) {
                                    case "1":
                                        njuga = parseInt(njuga) + 1;
                                        con = parseInt(resFinal);
                                        promCon1 = parseInt(promCon1) + parseInt(resFinal);
                                        break;
                                    case "2":
                                        aco = parseInt(resFinal);
                                        promAuto1 = parseInt(promAuto1) + parseInt(resFinal);
                                        break;
                                    case "3":
                                        ces = parseInt(resFinal);
                                        promEst1 = parseInt(promEst1) + parseInt(resFinal);
                                        break;
                                    case "4":
                                        act = parseInt(resFinal);
                                        promAct1 = parseInt(promAct1) + parseInt(resFinal);
                                        break;
                                    case "5":
                                        mot = parseInt(resFinal);
                                        promMot1 = parseInt(promMot1) + parseInt(resFinal);
                                        break;    
                                    case "6":
                                        cem = parseInt(resFinal);
                                        promCemo1 = parseInt(promCemo1) + parseInt(resFinal);
                                        break;
                                    case "7":
                                        obj = parseInt(resFinal);
                                        promObj1 = parseInt(promObj1) + parseInt(resFinal);
                                        break;        
                                    default:
                                        con = 0;
                                        aco = 0;
                                        ces = 0;
                                        act = 0;
                                        cem = 0;
                                        mot = 0;
                                        obj = 0;
                                    break;
                                }
                                co = co+con;
                                ce = ce+ces;
                                ac = ac+act;
                                af = af+aco;
                                cl = cl+cem;
                                mo = mo+mot;
                                ob = ob+obj;
                            }else if(a == "3"){
                                switch (vaid) {
                                    case "1":
                                        njuga = parseInt(njuga) + 1;
                                        con2 = parseInt(resFinal2);
                                        promCon2 = parseInt(promCon2) + parseInt(resFinal2);
                                        break;
                                    case "2":
                                        aco2 = parseInt(resFinal2);
                                        promAuto2 = parseInt(promAuto2) + parseInt(resFinal2);
                                        break;
                                    case "3":
                                        ces2 = parseInt(resFinal2);
                                        promEst2 = parseInt(promEst2) + parseInt(resFinal2);
                                        break;
                                    case "4":
                                        act2 = parseInt(resFinal2);
                                        promAct2 = parseInt(promAct2) + parseInt(resFinal2);
                                        break;
                                    case "5":
                                        mot2 = parseInt(resFinal2);
                                        promMot2 = parseInt(promMot2) + parseInt(resFinal2);
                                        break;    
                                    case "6":
                                        cem2 = parseInt(resFinal2);
                                        promCemo2 = parseInt(promCemo2) + parseInt(resFinal2);
                                        break;
                                    case "7":
                                        obj2 = parseInt(resFinal2);
                                        promObj2 = parseInt(promObj2) + parseInt(resFinal2);
                                        break;        
                                    default:
                                        con2 = 0;
                                        aco2 = 0;
                                        ces2 = 0;
                                        act2 = 0;
                                        cem2 = 0;
                                        mot2 = 0;
                                        obj2 = 0;
                                    break;
                                }
                                co2 = co2+con2;
                                ce2 = ce2+ces2;
                                ac2 = ac2+act2;
                                af2 = af2+aco2;
                                cl2 = cl2+cem2;
                                mo2 = mo2+mot2;
                                ob2 = ob2+obj2;
                            }else{
                                switch (vaid) {
                                    case "8":
                                        njuga = parseInt(njuga) + 1;
                                        rfi = parseInt(resFinal);
                                        promCfis = parseInt(promCfis)+parseInt(resFinal);
                                        break;
                                    case "9":
                                        hml = parseInt(resFinal);
                                        promHaMe = parseInt(promHaMe)+parseInt(resFinal);
                                        break;
                                    case "10":
                                        cat = parseInt(resFinal);
                                        promCapTec = parseInt(promCapTec)+parseInt(resFinal);
                                        break;
                                    case "11":
                                        tna = parseInt(resFinal);
                                        promTal = parseInt(promTal)+parseInt(resFinal);
                                        break;
                                    default:
                                        rfi = 0;
                                        hml = 0;
                                        cat = 0;
                                        tna = 0;
                                    break;
                                }
                                ro = ro+rfi;
                                hl = hl+hml;
                                ct = ct+cat;
                                tn = tn+tna;
                            }
                        }  
                    });
                    if(a == "1"){
                        promEntr = promEntr / countVar;
                        promCom = promCom / countVar;
                        promEntr = Math.round(promEntr);
                        promCom = Math.round(promCom);
                        console.log(promEntr+" / "+promCom);
                        new Chart(document.getElementById("radar-chart"), {
                            type: 'polarArea',
                            data: {
                                labels: ["Competencias", "Entrenamiento"],
                                datasets: [
                                    {
                                        label: "Competencias",
                                        fill: true,
                                        backgroundColor: "rgba(254, 209, 48,0.2)",
                                        borderColor: "rgba(254, 209, 48,1)",
                                        pointLabelFontSize: 16,
                                        pointBorderColor: "#fff",
                                        pointBackgroundColor: "rgba(254, 209, 48,1)",
                                        responsive: false,
                                        pointDot:false,
                                        showTooltips: false,
                                        scaleOverride: true,
                                        scaleSteps: 4,
                                        scaleStepWidth: 5,
                                        scaleStartValue: 0,
                                        data: [promCom,promEntr]
                                    }, {
                                        label: "Entrenamientos",
                                        fill: true,
                                        backgroundColor: "rgba(26, 123, 253,0.2)",
                                        borderColor: "rgba(26, 123, 253,1)",
                                        pointBorderColor: "#fff",
                                        pointBackgroundColor: "rgba(26, 123, 253,1)",
                                        pointBorderColor: "#fff",
                                        responsive: false,
                                        pointDot:false,
                                        showTooltips: false,
                                        scaleOverride: true,
                                        scaleSteps: 4,
                                        scaleStepWidth: 5,
                                        scaleStartValue: 0,
                                        data: [promCom,promEntr]
                                    }
                                ]
                            },
                            options: {
                                responsive: false,
                                title: {
                                    display: true,
                                    text: 'Rendimiento General'
                                },
                                legend: {
                                    display: false
                                }
                            }
                        });
                    }else if(a == "2"){
                        promCon1 = promCon1 / njuga;
                        promAuto1= promAuto1 / njuga;
                        promEst1 = promEst1 / njuga;
                        promAct1 = promAct1 / njuga;
                        promMot1 = promMot1 / njuga;
                        promCemo1 = promCemo1 / njuga;
                        promObj1 = promObj1 / njuga;
                        promCon1 = Math.round(promCon1);
                        promAuto1 = Math.round(promAuto1);
                        promEst1 = Math.round(promEst1);
                        promAct1 = Math.round(promAct1);
                        promMot1 = Math.round(promMot1);
                        promCemo1 = Math.round(promCemo1);
                        promObj1 = Math.round(promObj1);
                        new Chart(document.getElementById("radar-chart"), {
                            type: 'radar',
                            data: {
                                labels: ["Control de Estrés", "Activación", "Concentración", "Control Emocional", "Auto Confianza", "Motivación", "Objetivos"],
                                datasets: [
                                    {
                                        label: "Entrenamientos",
                                        fill: true,
                                        backgroundColor: "rgba(26, 123, 253,0.2)",
                                        borderColor: "rgba(26, 123, 253,1)",
                                        pointLabelFontSize: 16,
                                        pointBorderColor: "#fff",
                                        pointBackgroundColor: "rgba(26, 123, 253,1)",
                                        responsive: false,
                                        pointDot:false,
                                        showTooltips: false,
                                        scaleOverride: true,
                                        scaleSteps: 4,
                                        scaleStepWidth: 5,
                                        scaleStartValue: 0,
                                        data: [promEst1, promAct1, promCon1, promCemo1, promAuto1, promMot1, promObj1]
                                    }
                                ]
                            },
                            options: {
                                responsive: false,
                                title: {
                                    display: true,
                                    text: 'Resultados'
                                },
                                legend: {
                                    display: false
                                }
                            }
                        });
                    }else if(a == "3"){
                        promCon2 = promCon2 / njuga;
                        promAuto2= promAuto2 / njuga;
                        promEst2 = promEst2 / njuga;
                        promAct2 = promAct2 / njuga;
                        promMot2 = promMot2 / njuga;
                        promCemo2 = promCemo2 / njuga;
                        promObj2 = promObj2 / njuga;
                        promCon2 = Math.round(promCon2);
                        promAuto2 = Math.round(promAuto2);
                        promEst2 = Math.round(promEst2);
                        promAct2 = Math.round(promAct2);
                        promMot2 = Math.round(promMot2);
                        promCemo2 = Math.round(promCemo2);
                        promObj2 = Math.round(promObj2);
                        new Chart(document.getElementById("radar-chart"), {
                            type: 'radar',
                            data: {
                                labels: ["Control de Estrés", "Activación", "Concentración", "Control Emocional", "Auto Confianza", "Motivación", "Objetivos"],
                                datasets: [
                                    {
                                        label: "Competencias",
                                        fill: true,
                                        backgroundColor: "rgba(254, 209, 48,0.2)",
                                        borderColor: "rgba(254, 209, 48,1)",
                                        pointLabelFontSize: 16,
                                        pointBorderColor: "#fff",
                                        pointBackgroundColor: "rgba(254, 209, 48,1)",
                                        responsive: false,
                                        pointDot:false,
                                        showTooltips: false,
                                        scaleOverride: true,
                                        scaleSteps: 4,
                                        scaleStepWidth: 5,
                                        scaleStartValue: 0,
                                        data: [promEst2, promAct2, promCon2, promCemo2, promAuto2, promMot2, promObj2]
                                    }
                                ]
                            },
                            options: {
                                responsive: false,
                                title: {
                                    display: true,
                                    text: 'Resultados'
                                },
                                legend: {
                                    display: false
                                }
                            }
                        });
                    }else{
                        promCfis = promCfis / njuga;
                        promHaMe = promHaMe / njuga;
                        promCapTec = promCapTec / njuga;
                        promTal = promTal / njuga;
                        promCfis = Math.round(promCfis);
                        promHaMe = Math.round(promHaMe);
                        promCapTec = Math.round(promCapTec);
                        promTal = Math.round(promTal);
                        new Chart(document.getElementById("radar-chart"), {
                            type: 'radar',
                            data: {
                                labels: ["Rendimiento Físico", "Habilidad Mental", "Capacidades Técnicas", "Talento Natural"],
                                datasets: [
                                    {
                                        fill: true,
                                        backgroundColor: "rgba(21, 126, 251,0.2)",
                                        borderColor: "rgba(21, 126, 251,1)",
                                        pointLabelFontSize: 16,
                                        pointBorderColor: "#fff",
                                        pointBackgroundColor: "rgba(21, 126, 251,1)",
                                        responsive: false,
                                        pointDot:false,
                                        showTooltips: false,
                                        scaleOverride: true,
                                        scaleSteps: 4,
                                        scaleStepWidth: 5,
                                        scaleStartValue: 0,
                                        data: [promCfis, promHaMe, promCapTec, promTal]
                                    }
                                ]
                            },
                            options: {
                                responsive: false,
                                title: {
                                    display: true,
                                    text: 'Resultados'
                                },
                                legend: {
                                    display: false
                                }
                            }
                        });
                    }
                        
                }
            }            
        });
        /**FIN_AJAX**/
}

function pagarNeru(){
    window.location.href = "https://www.nerupsicologia.com/app/public/";
}

