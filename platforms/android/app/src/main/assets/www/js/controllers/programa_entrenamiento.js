
$( document ).on( "pageinit", "#page-pem", function( event ) {
    consultaVar();
    setTimeout(function(){ 
        var v_concentracion = localStorage.getItem('v_concentracion');
        var v_confianza = localStorage.getItem('v_confianza');
        var v_estres = localStorage.getItem('v_estres');
        var v_activacion = localStorage.getItem('v_activacion');
        var v_motivacion = localStorage.getItem('v_motivacion');
        var v_emocional = localStorage.getItem('v_emocional');
        var v_objetivos = localStorage.getItem('v_objetivos');
        var v_condicion = localStorage.getItem('v_condicion');
        var v_habilidad = localStorage.getItem('v_habilidad');
        var v_tecnicas = localStorage.getItem('v_tecnicas');
        var v_talento = localStorage.getItem('v_talento');
        localStorage.setItem('objetivoG','1');
        
        if(v_concentracion == "1"){
            $("#Concentracion").remove();
        }
        if(v_confianza == "1"){
            $("#AutoConfianza").remove();
        }
        if(v_estres == "1"){
            $("#Estresyansiedad").remove();
        }
        if(v_activacion == "1"){
            $("#Activacion").remove();
        }
        if(v_motivacion == "1"){
            $("#Motivacion").remove();
        }
        if(v_emocional == "1"){
            $("#Controlemocional").remove();
        }
        if(v_objetivos == "1"){
            $("#Objetivos").remove();
        }
        if(v_condicion == "1"){
            $("#Condicionfisica").remove();
        }
        if(v_habilidad == "1"){
            $("#HabilidadMental").remove();
        }
        if(v_tecnicas == "1"){
            $("#Capacidadestecnicas").remove();
        }
        if(v_talento == "1"){
            $("#Talento").remove();
        }
        if(v_talento == "1" && v_concentracion == "1" && v_estres == "1" && v_activacion == "1" && v_motivacion =="1" && v_emocional == "1" && v_objetivos == "1" && v_condicion == "1" && v_habilidad == "1" && v_tecnicas =="1"){
            $("#Objetivo").css("display","block");
        }
        cordova.plugin.pDialog.dismiss();
        $("#wizardInfo").steps({
            headerTag: "h2",
            bodyTag: "section",
            transitionEffect: "slideLeft"
        });
    }, 3000);
  });
$(document).on('pagebeforeshow', '#page-pem', function (e) {
    var pem = localStorage.getItem('var_pem');
    var v_concentracion = localStorage.getItem('v_concentracion');
    if (pem == 0 && v_concentracion == "0") {
        navigator.notification.alert(
            'Realiza tú evaluación',
            alertDismissed,
            'Inicio',
            'Aceptar',
        );
        
    }
    
    cordova.plugin.pDialog.init({
        theme : 'HOLO_DARK',
        progressStyle : 'SPINNER',
        cancelable : false,
        title : 'Espere por favor...',
        message : 'Cargando ...'
    });
    var idU = localStorage.getItem('id_usuario');
    var t3 = "variables_consulta," + pem + "," + idU + "," + new Date().getTime();
    /**AJAX**/
    $.ajax({
        url: 'https://www.nerupsicologia.com/app/Php/data.php',
        type: 'post',
        data: { datos: t3 },
        dataType: 'json',
        success: function (data) {
            if (data.success) {
                $.each(data, function (index, record) {
                    if ($.isNumeric(index)) {
                        var nom = record.nombre;
                            nom = nom.replace(/ /g, "")
                        $(".div_evaluacion").append("<div id='" + nom + "'><div class='title_evaluacion'>" +
                            "<h2>" + record.nombre + "</h2></div><div class='div_boton'>" +
                            "<button class='boton_info' id='btn_info_" + record.id_variable + "' onclick='view_variable(" + record.id_variable + ")'>Información</button>" +
                            "</div></div>");
                    }
                });
            } else {
                navigator.notification.alert(
                    data.msg,  // message
                    alertDismissed,         // callback
                    'Aviso',            // title
                    'Aceptar'                  // buttonName
                );
            }
        }
    });
    /**FIN_AJAX**/
    
    //******** */

    //Boton Información Concentración
    $(".btn_info").click(function () {
        $.mobile.changePage('pages/page-pem_info_concentra.html', {});
    });
    //Botón Evalua Concentración
    $("#btn_evalua_concentra").click(function () {
        $.mobile.changePage('pages/page-pem_evalua_concentra.html', {});
    });
    //Boton Información Activación
    $("#btn_info_activa").click(function () {
        $.mobile.changePage('pages/page-pem_evalua_concentra.html', {});
    });
    //Botón Evalua Activación
    $("#btn_evalua_activa").click(function () {
        $.mobile.changePage('pages/page-pem_evalua_concentra.html', {});
    });
    //Boton Información AutoConfianza
    $("#btn_info_autoconfia").click(function () {
        $.mobile.changePage('pages/page-pem_evalua_concentra.html', {});
    });
    //Botón Evalua AutoConfianza
    $("#btn_evalua_autoconfia").click(function () {
        $.mobile.changePage('pages/page-pem_evalua_concentra.html', {});
    });
    //Activida #
    $("#actividad_").click(function () {
        $.mobile.changePage('pages/page-pem_actividad.html', {});
    });
    $("#des_actividad_").click(function () {
        $.mobile.changePage('pages/page-pem_actividad_detalle.html', {});
    });
    //Regresar a Home
    $("#actividad_regresar").click(function () {
        $.mobile.changePage('pages/home.html', {});
    });
    //A INFO CONCENTRACIÓN
    $("#title_info_pem").click(function () {
        $.mobile.changePage('pages/page-descripcion_concentracion.html', {});
    });
    // Regresar a PEM
    $("#btn_regresar_pem").click(function () {
        $.mobile.changePage('pages/page-programa_entrenamiento.html', {});
    });
    //Boton Evaluación
    $("#btn_evalua_concentra").click(function () {
        $.mobile.changePage('pages/page-variables.html', {});
    });
    //VariableEvaluar
    $("#variable_concentracion").click(function () {
        $.mobile.changePage('pages/page-evalua.html', {});
    });

    /**FIN**/
});
/* PEM INFO CONCENTRA*/
$(document).on('pagebeforeshow', '#page-all_variable', function (event) {
    consultaVar();
});
$(document).on('pagebeforeshow', '#page-evaluacion', function (event) {
    consultaVar();
    navigator.notification.alert(
        "Toca la pantalla para agregar tu resultado",  // message
        alertando,         // callback
        'Aviso',            // title
        'Aceptar'                  // buttonName
    );
});
function guardar_evaluacion(idvar, ideva, r1, r2, tp, usuario) {
    var t3 = "guardar_evaluacion," + idvar + "," + ideva + "," + r1 + "," +r2+","+tp+","+ usuario + "," + new Date().getTime();        
    /**AJAX**/
    $.ajax({
        url: 'https://www.nerupsicologia.com/app/Php/data.php',
        type: 'post',
        data: { datos: t3 },
        dataType: 'json',
        success: function (data) {
            if (data.success) {
                switch (idvar) {
                    case "1":
                        localStorage.setItem("v_concentracion", "1");        
                    break;
                    case "2":
                        localStorage.setItem("v_confianza", "1");        
                    break;
                    case "3":
                        localStorage.setItem("v_estres", "1");        
                    break;
                    case "4":
                        localStorage.setItem("v_activacion", "1");        
                    break;
                    case "5":
                        localStorage.setItem("v_motivacion", "1");        
                    break;
                    case "6":
                        localStorage.setItem("v_emocional", "1");        
                    break;
                    case "7":
                        localStorage.setItem("v_objetivos", "1");        
                    break;
                    case "8":
                        localStorage.setItem("v_condicion", "1");        
                    break;
                    case "9":
                        localStorage.setItem("v_habilidad", "1");        
                    break;
                    case "10":
                        localStorage.setItem("v_tecnicas", "1");        
                    break;
                    case "11":
                        localStorage.setItem("v_talento", "1");        
                    break;
                }
                $.mobile.changePage('pages/page-programa_entrenamiento.html', {});
            } else {
                navigator.notification.alert(
                    data.msg,  // message
                    alertDismissed,         // callback
                    'Aviso',            // title
                    'Aceptar'                  // buttonName
                );
            }
        }
    });
    /**FIN_AJAX**/
}
function consultaVar(){
    var variableA = localStorage.getItem('variable_actual');
    switch (variableA) {
        case "1":
            $("#principal_div").addClass('concentracion_div');
            
            break;
        case "2":
            $("#principal_div").addClass('autoconfianza_div');
            break;
        case "3":
            $("#principal_div").addClass('estres_div');
            break;
        case "4":
            $("#principal_div").addClass('activacion_div');
            break;
        case "5":
            $("#principal_div").addClass('motivacion_div');
            break;
        case "6":
            $("#principal_div").addClass('control_div');
            break;
        case "7":
            $("#principal_div").addClass('objetivos_div');
            break;
        case "8":
            $("#principal_div").addClass('condicion_div');
            break;
        case "9":
            $("#principal_div").addClass('habilidad_div');
            break;
        case "10":
            $("#principal_div").addClass('tecnicas_div');
            break;
        default:
            $("#principal_div").addClass('talento_div');
    }    
    var resEva = 0;
        var resObj = 0;
        
        var variableA = localStorage.getItem('variable_actual');
        var t3 = "evaluacion_consulta," + variableA + "," + new Date().getTime();
        /*API*/
        /**AJAX**/
        $.ajax({
            url: 'https://www.nerupsicologia.com/app/Php/data.php',
            type: 'post',
            data: { datos: t3 },
            dataType: 'json',
            
            success: function (data) {
                var suma_preguntas = 0;
                var longi = Object.keys(data).length -2;
                var longiP = Object.keys(data).length - 3;
                if (data.success) {
                    $("#wizard").empty();
                    $.each(data, function (index, record) {
                        if ($.isNumeric(index)) {
                            var pregunta = record.pregunta;
                            var pregunta = pregunta.replace('?', '¿');
                            $("#wizard").append("<h2></h2><section><input type='hidden' id='tipoP' value="+index+"><div style='height:30%'><p style='color:black;height:15vh !important;font-size:14px'>"+record.pregunta+"</p></div>"+
                            "<div style='text-align:center'><input type='text' class='dial"+index+"' id='res"+index+"' data-min='0' data-max='10' value='0' data-fgColor='#06139a'"+
                            "data-angleOffset=-125 data-angleArc=250 style='visibility:hidden' '>"+
                            "<div class='tipo_text_evaluacion' id='text_eva"+index+"'></div></div>"+
                            "</section>");
                            $("#id_evaluacion").val(record.id_evaluacion);
                            if(index == "0"){
                                suma_preguntas = 1;
                                $("#pregunta_texto"+index).text(record.pregunta);
                            }else if(index == "1"){
                                suma_preguntas = suma_preguntas + 1;
                                $("#pregunta_texto"+index).text(record.pregunta);
                            }
                        }
                    });
                    if(suma_preguntas < 2){
                        $("#step2").remove();
                    }
                    $("#wizard").steps({
                        headerTag: "h2",
                        bodyTag: "section",
                        transitionEffect: "slideLeft"
                    });
                    $(".dial0").knob({
                        'change': function () {
                            //v.preventDefault;
                            var a = $("#res0").val();
                            test_captura(a,"dial0");
                        }
                    });
                    $(".dial1").knob({
                        'change': function () {
                            //v.preventDefault;
                            var b = $("#res1").val();
                            test_captura(b,"dial1");
                        }
                    });
                } else {
                    /*
                    navigator.notification.alert(
                        data.msg,  // message
                        alertDismissed,         // callback
                        'Aviso',            // title
                        'Aceptar'                  // buttonName
                    );
                    */
                }
            }
        });
        /**FIN_AJAX**/
        
}

function guardar_objetivos(usuario, objetivo) {
    var t3 = "guardar_objetivo," + usuario + "," + objetivo + "," + new Date().getTime();
    /**AJAX**/
    $.ajax({
        url: 'https://www.nerupsicologia.com/app/Php/data.php',
        type: 'post',
        data: { datos: t3 },
        dataType: 'json',
        success: function (data) {
            if (data.success) {
                localStorage.setItem("var_pem", "1");
                $.mobile.changePage('pages/page-variables.html', {});
            } else {
                navigator.notification.alert(
                    data.msg,  // message
                    alertDismissed,         // callback
                    'Aviso',            // title
                    'Aceptar'                  // buttonName
                );
            }
        }
    });
    /**FIN_AJAX**/
}
function view_variable(idV) {
    localStorage.setItem("variable_actual", idV);
    $.mobile.changePage('pages/variables/page_all_variable.html');
}
function view_objetivo() {
    $.mobile.changePage('pages/objetivos.html');
}
function test_variable() {
    var varsel = localStorage.getItem('variable_actual');
    $.mobile.changePage('pages/page-evaluacion.html');
}
function alertDismissed() {
    navigator.notification.alert(
        'Bienvenido a la evaluación de NERU, da click en información lee la información de cada variable y posteriormente da click en realizar evaluación, mucho Exito, entrena tu mente llega a tu NERU',  // message
        alertando,         // callback
        'Inicio',            // title
        'Aceptar'                  // buttonName
    );
}
function alertando() { } 

function test_captura(res, tipo){
    res = parseInt(res);
    $("#imgEstado").empty();
    var variableA = localStorage.getItem('variable_actual');
    var emocion="";
    var texto ="";
    if(tipo=="dial0"){
        $("#imgEstado").empty();
        if(variableA=="2" || variableA=="3" || variableA=="4" || variableA=="5"){
            $("#imgEstado").empty();
            if(res >= 0 && res < 3){
                $('.dial0').trigger('configure', {'fgColor':"#ff0000"});
                emocion = "baja";
                switch(variableA){
                    case "2":
                        texto = "AutoConfianza Baja";
                    break;
                    case "3":
                        texto = "Estrés y ansiedad Bajo";
                    break;
                    case "4":
                        texto = "Activación Baja";
                    break;
                    case "5":
                        texto = "Motivación Baja";
                    break;
                }
            }else if(res >= 3 && res < 5){
                $('.dial0').trigger('configure', {'fgColor':"#ffff00"});
                emocion = "media";
                switch(variableA){
                    case "2":
                        texto = "AutoConfianza Media";
                    break;
                    case "3":
                        texto = "Estrés y ansiedad Media";
                    break;
                    case "4":
                        texto = "Activación Media";
                    break;
                    case "5":
                        texto = "Motivación Media";
                    break;
                }
            }else if(res >= 5 && res < 7){
                $('.dial0').trigger('configure', {'fgColor':"#00ff00"});
                emocion = "optima";
                switch(variableA){
                    case "2":
                        texto = "AutoConfianza Optima";
                    break;
                    case "3":
                        texto = "Estrés y ansiedad Optimo";
                    break;
                    case "4":
                        texto = "Activación Optima";
                    break;
                    case "5":
                        texto = "Motivación Optima";
                    break;
                }
            }else if(res >= 7 && res < 9){
                $('.dial0').trigger('configure', {'fgColor':"#ffff00"});
                emocion = "alta";
                switch(variableA){
                    case "2":
                        texto = "AutoConfianza Alta";
                    break;
                    case "3":
                        texto = "Estrés y ansiedad Alto";
                    break;
                    case "4":
                        texto = "Activación Alta";
                    break;
                    case "5":
                        texto = "Motivación Alta";
                    break;
                }
            }else{
                $('.dial0').trigger('configure', {'fgColor':"#ff0000"});
                emocion = "exceso";
                switch(variableA){
                    case "2":
                        texto = "AutoConfianza en exceso";
                    break;
                    case "3":
                        texto = "Estrés y ansiedad en Exceso";
                    break;
                    case "4":
                        texto = "Activación en exceso";
                    break;
                    case "5":
                        texto = "Motivación en exceso";
                    break;
                }
            }
        }else{
            if (res >= 0 && res < 4) {
                $("#imgEstado").empty();
                $('.dial0').trigger('configure', {'fgColor':"#ff0004"});
                emocion = "baja";
                switch(variableA){
                    case "1":
                        texto = "Concentración Baja";
                    break;
                    case "6":
                        texto = "Poco Control emocional";
                    break;
                    case "7":
                        texto = "No Cumplo ningun Objetivos";
                    break;
                    case "8":
                        texto = "Poca Resistencia Física";
                    break;
                    case "9":
                        texto = "Poca Habilidad Mental";
                    break;
                    case "10":
                        texto = "Poca Capacidad técnica";
                    break;
                    case "11":
                        texto = "Poco Talento";
                    break;
                }
            }else if(res >= 4 && res < 7){
                $('.dial0').trigger('configure', {'fgColor':"#f6ff00"});
                emocion = "media";
                $("#imgEstado").empty();
                switch(variableA){
                    case "1":
                        texto = "Concentración Regular";
                    break;
                    case "6":
                        texto = "Control emocional Regular";
                    break;
                    case "7":
                        texto = "No Cumplo todos mis Objetivos";
                    break;
                    case "8":
                        texto = "Resistencia Física Regular";
                    break;
                    case "9":
                        texto = "Habilidad Mental Regular";
                    break;
                    case "10":
                        texto = "Capacidad técnica Regular";
                    break;
                    case "11":
                        texto = "Talento Regular";
                    break;
                }
            }else if(res >= 7 && res <= 8){
                $('.dial0').trigger('configure', {'fgColor':"#f6ff00"});
                emocion = "alta";
                $("#imgEstado").empty();
                switch(variableA){
                    case "1":
                        texto = "Buena Concentración";
                    break;
                    case "6":
                        texto = "Buen control emocional";
                    break;
                    case "7":
                        texto = "Cumplo con mis Objetivos";
                    break;
                    case "8":
                        texto = "Buena Resistencia Física";
                    break;
                    case "9":
                        texto = "Buena Habilidad Mental";
                    break;
                    case "10":
                        texto = "Buena Capacidad técnica";
                    break;
                    case "11":
                        texto = "Buen Talento";
                    break;
                }
            }else{
                $('.dial0').trigger('configure', {'fgColor':"#12d304"});
                emocion = "optima";
                $("#imgEstado").empty();
                switch(variableA){
                    case "1":
                        texto = "Excelente Concentración";
                    break;
                    case "6":
                        texto = "Excelente control emocional";
                    break;
                    case "7":
                        texto = "Cumplo Excelente con mis Objetivos";
                    break;
                    case "8":
                        texto = "Excelente Resistencia Física";
                    break;
                    case "9":
                        texto = "Excelente Habilidad Mental";
                    break;
                    case "10":
                        texto = "Excelente Capacidad técnica";
                    break;
                    case "11":
                        texto = "Excelente Talento";
                    break;
                }
            }
        }
        setTimeout(function(){ 
            $("#imgEstado").empty();
            $("#text_eva0").text(texto);
            $("#imgEstado").append('<img src="./img/variables/'+emocion+'.png" width="100" height="95" style="margin-top:5vh">');
        }, 500);
        $("#text_eva0").text(texto);
        //$("#imgEstado").append('<img src="./img/variables/'+emocion+'.png" width="100" height="100">');
    }else{//Dial 1
        $("#imgEstado").empty();
        if(variableA=="2" || variableA=="3" || variableA=="4" || variableA=="5"){
            $("#imgEstado").empty();
            if(res >= 0 && res < 3){
                $('.dial1').trigger('configure', {'fgColor':"#ff0000"});
                emocion = "baja";
                switch(variableA){
                    case "2":
                        texto = "AutoConfianza Baja";
                    break;
                    case "3":
                        texto = "Estrés y ansiedad Bajo";
                    break;
                    case "4":
                        texto = "Activación Baja";
                    break;
                    case "5":
                        texto = "Motivación Baja";
                    break;
                }
            }else if(res >= 3 && res < 5){
                $('.dial1').trigger('configure', {'fgColor':"#ffff00"});
                emocion = "media";
                switch(variableA){
                    case "2":
                        texto = "AutoConfianza Media";
                    break;
                    case "3":
                        texto = "Estrés y ansiedad Media";
                    break;
                    case "4":
                        texto = "Activación Media";
                    break;
                    case "5":
                        texto = "Motivación Media";
                    break;
                }
            }else if(res >= 5 && res < 7){
                $('.dial1').trigger('configure', {'fgColor':"#00ff00"});
                emocion = "optima";
                switch(variableA){
                    case "2":
                        texto = "AutoConfianza Optima";
                    break;
                    case "3":
                        texto = "Estrés y ansiedad Optimo";
                    break;
                    case "4":
                        texto = "Activación Optima";
                    break;
                    case "5":
                        texto = "Motivación Optima";
                    break;
                }
            }else if(res >= 7 && res < 9){
                $('.dial1').trigger('configure', {'fgColor':"#ffff00"});
                emocion = "alta";
                switch(variableA){
                    case "2":
                        texto = "AutoConfianza Alta";
                    break;
                    case "3":
                        texto = "Estrés y ansiedad Alto";
                    break;
                    case "4":
                        texto = "Activación Alta";
                    break;
                    case "5":
                        texto = "Motivación Alta";
                    break;
                }
            }else{
                $('.dial1').trigger('configure', {'fgColor':"#ff0000"});
                emocion = "exceso";
                switch(variableA){
                    case "2":
                        texto = "AutoConfianza en exceso";
                    break;
                    case "3":
                        texto = "Estrés y ansiedad en Exceso";
                    break;
                    case "4":
                        texto = "Activación en exceso";
                    break;
                    case "5":
                        texto = "Motivación en exceso";
                    break;
                }
            }
        }else{
            if (res >= 0 && res < 4) {
                $("#imgEstado").empty();
                $('.dial1').trigger('configure', {'fgColor':"#ff0004"});
                emocion = "baja";
                switch(variableA){
                    case "1":
                        texto = "Concentración Baja";
                    break;
                    case "6":
                        texto = "Poco Control emocional";
                    break;
                    case "7":
                        texto = "No Cumplo ningun Objetivos";
                    break;
                    case "8":
                        texto = "Poca Resistencia Física";
                    break;
                    case "9":
                        texto = "Poca Habilidad Mental";
                    break;
                    case "10":
                        texto = "Poca Capacidad técnica";
                    break;
                    case "11":
                        texto = "Poco Talento";
                    break;
                }
            }else if(res >= 4 && res < 7){
                $('.dial1').trigger('configure', {'fgColor':"#f6ff00"});
                emocion = "media";
                switch(variableA){
                    case "1":
                        texto = "Concentración Regular";
                    break;
                    case "6":
                        texto = "Control emocional Regular";
                    break;
                    case "7":
                        texto = "No Cumplo todos mis Objetivos";
                    break;
                    case "8":
                        texto = "Resistencia Física Regular";
                    break;
                    case "9":
                        texto = "Habilidad Mental Regular";
                    break;
                    case "10":
                        texto = "Capacidad técnica Regular";
                    break;
                    case "11":
                        texto = "Talento Regular";
                    break;
                }
            }else if(res >= 7 && res <= 8){
                $('.dial1').trigger('configure', {'fgColor':"#f6ff00"});
                emocion = "alta";
                switch(variableA){
                    case "1":
                        texto = "Buena Concentración";
                    break;
                    case "6":
                        texto = "Buen control emocional";
                    break;
                    case "7":
                        texto = "Cumplo con mis Objetivos";
                    break;
                    case "8":
                        texto = "Buena Resistencia Física";
                    break;
                    case "9":
                        texto = "Buena Habilidad Mental";
                    break;
                    case "10":
                        texto = "Buena Capacidad técnica";
                    break;
                    case "11":
                        texto = "Buen Talento";
                    break;
                }
            }else{
                $('.dial1').trigger('configure', {'fgColor':"#12d304"});
                emocion = "optima";
                switch(variableA){
                    case "1":
                        texto = "Excelente Concentración";
                    break;
                    case "6":
                        texto = "Excelente control emocional";
                    break;
                    case "7":
                        texto = "Cumplo Excelente con mis Objetivos";
                    break;
                    case "8":
                        texto = "Excelente Resistencia Física";
                    break;
                    case "9":
                        texto = "Excelente Habilidad Mental";
                    break;
                    case "10":
                        texto = "Excelente Capacidad técnica";
                    break;
                    case "11":
                        texto = "Excelente Talento";
                    break;
                }
            }
        }
        setTimeout(function(){ 
            $("#imgEstado").empty();
            $("#text_eva1").text(texto);
            $("#imgEstado").append('<img src="./img/variables/'+emocion+'.png" width="100" height="95" style="margin-top:5vh !important">');
        }, 100);

    }

}