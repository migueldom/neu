
$(document).on('pagebeforeshow', '#page-perfil', function (e) {

    var imgP = localStorage.getItem("imgData");
    if(imgP.length > 1){
        $("#imgPerfil").css('display','block');
    }
    /** */
        // Get all variables
var bannerImage = document.getElementById('bannerImg');
var result = document.getElementById('res');
var img = document.getElementById('imgPerfil');

// 
bannerImage.addEventListener('change', function() {
    $("#imgPerfil").css('display','block');
    var file = this.files[0];
    // declare a maxSize (3Mb)
    var maxSize = 3000000;

    if (file.type.indexOf('image') < 0) {
        res.innerHTML = 'invalid type';
        return;
    }
    var fReader = new FileReader();
    fReader.onload = function() {
    	img.onload = function(){
    		// if localStorage fails, it should throw an exception
			try{
				// pass the ratio of the file size/maxSize to your toB64 func in case we're already out of scope
				localStorage.setItem("imgData", getBase64Image(img, (file.size/maxSize), file.type));
        		}
			catch(e){
				var msg = e.message.toLowerCase();
				// We exceeded the localStorage quota
				if(msg.indexOf('storage')>-1 || msg.indexOf('quota')>-1){
					// we're dealing with a jpeg image :  try to reduce the quality
					if(file.type.match(/jpe?g/)){
						localStorage.setItem("imgData", getBase64Image(img, (file.size/maxSize), file.type, 0.7));
						}
					// we're dealing with a png image :  try to reduce the size
					else{
						// maxSize is a total approximation I got from some tests with a random pixel generated img
						var maxPxSize = 750000,
						imgSize = (img.width*img.height);
						localStorage.setItem("imgData", getBase64Image(img, (imgSize/maxPxSize), file.type));
						}
					}
				}
			}
        img.src = fReader.result;
    };
    
    fReader.readAsDataURL(file);
});

function getBase64Image(img, sizeRatio, type, quality) {
	// if we've got an svg, don't convert it, svg will certainly be less big than any pixel image
	if(type.indexOf('svg+xml')>0) return img.src;

	// if we've got a jpeg
	if(type.match(/jpe?g/)){
		// and the sizeRatio is okay, don't convert it
		if(sizeRatio<=1) return img.src;
		}
	// if we've got some other image type
	else type = 'image/png';

	if(!quality) quality = 1;
    var canvas = document.createElement("canvas");
	// if our image file is too large, then reduce its size
    canvas.width = (sizeRatio>1)?(img.width/sizeRatio): img.width;
    canvas.height = (sizeRatio>1)?(img.height/sizeRatio): img.height;

    var ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
    // if we already tried to reduce its size but it's still failing, then reduce the jpeg quality
    var dataURL = canvas.toDataURL(type, quality);
    
    return dataURL;
}

function fetchimage () {
    var dataImage = localStorage.getItem('imgData');
    if(dataImage.length == 0 || dataImage == null || dataImage == "null"){
        $("#imgPerfil").css('display','none');
    }else{
        $("#imgPerfil").css('display','block');
        img.src = dataImage;
    }
}

// Call fetch to get image from localStorage.
fetchimage();
    /** */
   
    var usuario = localStorage.getItem("id_usuario");
    localStorage.removeItem('id_jugador_actual');
    var t3 = "resultados_evaluacion," + usuario + "," + new Date().getTime();
    /**AJAX**/
    $.ajax({
        url: 'https://www.nerupsicologia.com/app/Php/data.php',
        type: 'post',
        data: { datos: t3 },
        dataType: 'json',
        success: function (data) {
            console.log(data);
            if (data.success) {
                $.each(data, function (index, record) {
                    if ($.isNumeric(index)) {
                        var variable_c = record.variable;
                        var vaid = record.id_variable;
                        $("#nombre_jugador").text(record.nombre+" "+record.app+" "+record.apm);
                        $("#edad").val(record.edad);
                        $("#ciudad").val(record.ciudad);
                        $("#posicion").val(record.posicion);
                        $("#fecha_nacimiento").val(record.fecha_nacimiento);
                        $("#sexo").val(record.genero).change();
                        /*$("#popupBasic").append("<div style='text-align:center;color:black'><h3>"+record.variable+"</h3><p id='pregunta'>"+record.pregunta+"</p>"+
                        "<span><b>"+resFinal+"</b></span></div>");*/
                    }
                });
            } else {
                navigator.notification.alert(
                    data.msg,  // message
                    alertDismissed,         // callback
                    'Aviso',            // title
                    'Aceptar'                  // buttonName
                );
            }
        }
    });
    /**FIN_AJAX**/
    /**AJAX**/
    var t4 = "resultados_objetivos," + usuario + "," + new Date().getTime();
    $.ajax({
        url: 'https://www.nerupsicologia.com/app/Php/data.php',
        type: 'post',
        data: { datos: t4 },
        dataType: 'json',
        success: function (data) {
            if (data.success) {
                $.each(data, function (index, record) {
                    if ($.isNumeric(index)) {
                        var tipoObj = "";
                        var des_obj = "";
                        $("#ul_obj").append("<li><i class='fas fa-caret-right'></i>" +
                            "<b>Objetivo General a Mediano Plazo</b><p>" + record.objetivo + "</p></li>" +
                            "<li><i class='fas fa-caret-right'></i>" +
                            "<b>Objetivo General a Largo Plazo</b><p>" + record.objetivo_gen_lp + "</p></li>" +
                            "<li><i class='fas fa-caret-right'></i>" +
                            "<b>Objetivos Especificos a Mediano Plazo (Físico)</b><p>" + record.objetivo_lp_fisico + "</p></li>" +
                            "<li><i class='fas fa-caret-right'></i>" +
                            "<b>Objetivos Especificos a Mediano Plazo (Técnico)</b><p>" + record.objetivo_lp_tecnicos + "</p></li>" +
                            "<li><i class='fas fa-caret-right'></i>" +
                            "<b>Objetivos Especificos a Mediano Plazo (Psicológicos)</b><p>" + record.objetivo_lp_psicologicos + "</p></li>" +
                            "<li><i class='fas fa-caret-right'></i>" +
                            "<b>Objetivos Especificos a Largo Plazo (Físico)</b><p>" + record.objetivo_mp_fisico + "</p></li>" +
                            "<li><i class='fas fa-caret-right'></i>" +
                            "<b>Objetivos Especificos a Largo Plazo (Técnico)</b><p>" + record.objetivo_mp_tecnicos + "</p></li>" +
                            "<li><i class='fas fa-caret-right'></i>" +
                            "<b>Objetivos Especificos a Largo Plazo (Psicológicos})</b><p>" + record.objetivo_mp_psicologicos + "</p></li>");
                    }
                });
            }
        }
    });
    /**FIN_AJAX**/
});

function actualiza_jugador(){
    cordova.plugin.pDialog.init({
        theme : 'HOLO_DARK',
        progressStyle : 'SPINNER',
        cancelable : true,
        title : 'Espere por favor...',
        message : 'Cargando ...'
    });
    var usuario = localStorage.getItem("id_usuario");
    var edad = $("#edad").val();
    var ciudad = $("#ciudad").val();
    var pocision = $("#posicion").val();
    var fecha_nacimiento = $("#fecha_nacimiento").val();
    var imagen = localStorage.getItem("imgData");
    var codequipo = $("#codigo_equipo_perfil").val();
    if(fecha_nacimiento.length == 0){
        navigator.notification.alert(
            'Falta Agregar Fecha Nacimiento',  // message
            alertDismissedN,         // callback
            'Aviso',            // title
            'Aceptar'                  // buttonName
        );
        return;
    }
    var sexo = $('#sexo option:selected').val() == undefined ? 'N' : $('#sexo option:selected').val()
    var t3 = "actualiza_usuario,"+usuario+","+edad+","+ciudad+","+pocision+","+fecha_nacimiento+","+sexo+","+imagen+","+codequipo+","+ new Date().getTime();;
    $.ajax({
        url: 'https://www.nerupsicologia.com/app/Php/data.php',
        type: 'post',
        data: { datos: t3 },
        dataType: 'json',
        success: function (data) {
            cordova.plugin.pDialog.dismiss();
            if (data.success) {
                $.mobile.changePage('pages/page-perfil.html', {});
            } else {
                
            }
        }
    });
}
$(document).on('pagebeforeshow', '#page-pem_grafica', function (e) {
    ver_grafica_perfil(1);
    localStorage.setItem("graficaInd",'1');
});
$(document).on('pagebeforeshow', '#page-pem_grafica_ind', function (e) {
    ver_grafica_perfil(1);
    localStorage.setItem("graficaInd",'0');
});
function alertDismissedHome() {
    $.mobile.changePage('pages/home.html', {});
}
function alertDismissed() {}
function alertDismissedN(){
}

function ver_grafica(){
    var status_pago = localStorage.getItem("pago");
    if (status_pago == "0") {
        navigator.notification.alert(
            "Debes suscribirte para poder acceder a esta sección",  // message
            alertDismissedN,         // callback
            'Aviso',            // title
            'Aceptar'                  // buttonName
        );
        
        //return;
    }else{
        $.mobile.changePage('pages/page-pem_grafica.html', {});
    }
}

function ver_grafica_i(){
    var status_pago = localStorage.getItem("pago");
    if (status_pago == "0") {
        navigator.notification.alert(
            "Debes suscribirte para poder acceder a esta sección",  // message
            alertDismissedd,         // callback
            'Aviso',            // title
            'Aceptar'                  // buttonName
        );
        
        //return;
    }else{
        $.mobile.changePage('pages/page-pem_grafica_ind.html', {});
    }
}

function imageChange(){
}

$(document).on('pagebeforeshow', '#page-perfil_individual', function (e) {
    var dataImage = localStorage.getItem('imgData');
    if(dataImage.length == 0 || dataImage == null || dataImage == "null"){
        $("#imgPerfil").css('display','none');
    }else{
        $("#imgPerfil").css('display','block');
        img.src = dataImage;
    }
    var usuario = localStorage.getItem("id_jugador_actual");
    var t3 = "resultados_evaluacion," + usuario + "," + new Date().getTime();
    /**AJAX**/
    $.ajax({
        url: 'https://www.nerupsicologia.com/app/Php/data.php',
        type: 'post',
        data: { datos: t3 },
        dataType: 'json',
        success: function (data) {
            if (data.success) {
                $.each(data, function (index, record) {
                    if ($.isNumeric(index)) {
                        //var variable_c = record.variable;
                        var vaid = record.id_variable;
                        $("#nombre_jugador").text(record.nombre+" "+record.app+" "+record.apm);
                        $("#edad").val(record.edad);
                        $("#ciudad").val(record.ciudad);
                        $("#posicion").val(record.posicion);
                        $("#fecha_nacimiento").val(record.fecha_nacimiento);
                        $("#sexo").val(record.genero).change();
                    }
                });
            } else {
                navigator.notification.alert(
                    data.msg,  // message
                    alertDismissed,         // callback
                    'Aviso',            // title
                    'Aceptar'                  // buttonName
                );
            }
        }
    });
    /**FIN_AJAX**/
    /**AJAX**/
    var t4 = "resultados_objetivos," + usuario + "," + new Date().getTime();
    $.ajax({
        url: 'https://www.nerupsicologia.com/app/Php/data.php',
        type: 'post',
        data: { datos: t4 },
        dataType: 'json',
        success: function (data) {
            if (data.success) {
                $.each(data, function (index, record) {
                    if ($.isNumeric(index)) {
                        var tipoObj = "";
                        var des_obj = "";
                        $("#ul_obj").append("<li><i class='fas fa-caret-right'></i>" +
                            "<b>Objetivo General a Mediano Plazo</b><p>" + record.objetivo + "</p></li>" +
                            "<li><i class='fas fa-caret-right'></i>" +
                            "<b>Objetivo General a Largo Plazo</b><p>" + record.objetivo_gen_lp + "</p></li>" +
                            "<li><i class='fas fa-caret-right'></i>" +
                            "<b>Objetivos Especificos a Mediano Plazo (Físico)</b><p>" + record.objetivo_lp_fisico + "</p></li>" +
                            "<li><i class='fas fa-caret-right'></i>" +
                            "<b>Objetivos Especificos a Mediano Plazo (Técnico)</b><p>" + record.objetivo_lp_tecnicos + "</p></li>" +
                            "<li><i class='fas fa-caret-right'></i>" +
                            "<b>Objetivos Especificos a Mediano Plazo (Psicológicos)</b><p>" + record.objetivo_lp_psicologicos + "</p></li>" +
                            "<li><i class='fas fa-caret-right'></i>" +
                            "<b>Objetivos Especificos a Largo Plazo (Físico)</b><p>" + record.objetivo_mp_fisico + "</p></li>" +
                            "<li><i class='fas fa-caret-right'></i>" +
                            "<b>Objetivos Especificos a Largo Plazo (Técnico)</b><p>" + record.objetivo_mp_tecnicos + "</p></li>" +
                            "<li><i class='fas fa-caret-right'></i>" +
                            "<b>Objetivos Especificos a Largo Plazo (Psicológicos})</b><p>" + record.objetivo_mp_psicologicos + "</p></li>");
                    }
                });
            }
        }
    });
    /**FIN_AJAX**/ 
});

function envCod(){
    cargarLoad();
    var ce = $("#codigo_equipo_perfil").val();
    var usuario = localStorage.getItem("id_usuario");
    var t3 = "add_codigo_usuario,"+usuario+","+ce+","+ new Date().getTime();;
    $.ajax({
        url: 'https://www.nerupsicologia.com/app/Php/data.php',
        type: 'post',
        data: { datos: t3 },
        dataType: 'json',
        success: function (data) {
            cordova.plugin.pDialog.dismiss();
            if (data.success) {
                navigator.notification.alert(
                    'Datos Actualizados Correctamente',  // message
                    alertDismissed,         // callback
                    'Aviso',            // title
                    'Aceptar'                  // buttonName
                );
                $.mobile.changePage('pages/home.html', {});
            } else {
                navigator.notification.alert(
                    data.msg,  // message
                    alertDismissed,         // callback
                    'Aviso',            // title
                    'Aceptar'                  // buttonName
                );
            }
        }
    });
}

function ver_grafica_perfil(a){
    $('#radars').remove(); // this is my <canvas> element
    $('#div_grafica').append('<canvas id="radars" width="280" height="430"></canvas>');
    var usuario = localStorage.getItem("id_jugador_actual");
    if(usuario == null){
        usuario = localStorage.getItem("id_usuario");
    }
    var conc1, auto1, estres1, acti1, moti1, emocion1, obj1 = 0;
    var conce2, auto2, estres2, acti2, moti2, emocion2, obj2, condi2, mental2, tecnica2 = 0;
    var talento2 = 0;
    var t3 = "resultados_evaluacion," + usuario + "," + new Date().getTime();
    /**AJAX**/
    $.ajax({
        url: 'https://www.nerupsicologia.com/app/Php/data.php',
        type: 'post',
        data: { datos: t3 },
        dataType: 'json',
        success: function (data) {
            if (data.success) {
                var resFinal = "";
                var variable = "";
                $.each(data, function (index, record) {
                    if ($.isNumeric(index)) {
                        var variable_c = record.variable;
                        var vaid = record.id_variable;
                        if (variable == variable_c) {
                            /*Partido*/
                            resFinal = record.resultado2;
                            if(resFinal=='undefined'){resFinal = 0;}
                            switch (vaid) {
                                case "1":
                                    conc1 = parseInt(resFinal);
                                    break;
                                case "2":
                                    auto1 = parseInt(resFinal);
                                    break;
                                case "3":
                                    estres1 = parseInt(resFinal);
                                    break;
                                case "4":
                                    acti1 = parseInt(resFinal);
                                    break;
                                case "5":
                                    moti1 = parseInt(resFinal);
                                    break;
                                case "6":
                                    emocion1 = parseInt(resFinal);
                                    break;
                                case "7":
                                    obj1 = parseInt(resFinal);
                                    break;
                                case "8":
                                    condi1 = parseInt(resFinal);
                                    break;
                                case "9":
                                    mental1 = parseInt(resFinal);
                                    break;
                                case "10":
                                    tecnica1 = parseInt(resFinal);
                                    break;
                                case "11":
                                    talento1 = parseInt(resFinal);
                                    break;
                            }
                        } else {
                            resFinal = record.resultado;
                            if(resFinal=='undefined'){resFinal = 0;}
                            variable = record.variable;
                            /*Entrenamiento*/
                            switch (vaid) {
                                case "1":
                                    conce2 = parseInt(resFinal);
                                    break;
                                case "2":
                                    auto2 = parseInt(resFinal);
                                    break;
                                case "3":
                                    estres2 = parseInt(resFinal);
                                    break;
                                case "4":
                                    acti2 = parseInt(resFinal);
                                    break;
                                case "5":
                                    moti2 = parseInt(resFinal);
                                    break;
                                case "6":
                                    emocion2 = parseInt(resFinal);
                                    break;
                                case "7":
                                    obj2 = parseInt(resFinal);
                                    break;
                                case "8":
                                    condi2 = parseInt(resFinal);
                                    break;
                                case "9":
                                    mental2 = parseInt(resFinal);
                                    break;
                                case "10":
                                    tecnica2 = parseInt(resFinal);
                                    break;
                                case "11":
                                    talento2 = parseInt(resFinal);
                                    break;
                            }
                        }

                        /*$("#popupBasic").append("<div style='text-align:center;color:black'><h3>"+record.variable+"</h3><p id='pregunta'>"+record.pregunta+"</p>"+
                        "<span><b>"+resFinal+"</b></span></div>");*/
                    }
                });
                if(a == "1"){
                    new Chart(document.getElementById("radars"), {
                        type: 'radar',
                        data: {
                            labels: ["Concentración", "AutoConfianza", "Estres/Ansiedad", "Activación", "Motivación", "ControlEmocional", "Objetivos"],
                            datasets: [
                                {
                                    label: "Competencias",
                                    fill: true,
                                    backgroundColor: "rgba(179,181,198,0.2)",
                                    borderColor: "rgba(179,181,198,1)",
                                    pointLabelFontSize: 16,
                                    pointBorderColor: "#fff",
                                    pointBackgroundColor: "rgba(179,181,198,1)",
                                    responsive: false,
                                    pointDot:false,
                                    showTooltips: false,
                                    scaleOverride: true,
                                    scaleSteps: 4,
                                    scaleStepWidth: 5,
                                    scaleStartValue: 0,
                                    data: [conc1, auto1, estres1, acti1, moti1, emocion1, obj1]
                                }
                            ]
                        },
                        options: {
                            responsive: false,
                            title: {
                                display: true,
                                text: 'Resultados de evaluación por Competencias'
                            },
                            legend: {
                                display: false
                            }
                        }
                    });
                }else if(a == "2"){
                    new Chart(document.getElementById("radars"), {
                        type: 'radar',
                        data: {
                            labels: ["Concentración", "AutoConfianza", "Estres/Ansiedad", "Activación", "Motivación", "ControlEmocional", "Objetivos"],
                            datasets: [
                                {
                                    label: "Entrenamientos",
                                    fill: true,
                                    backgroundColor: "rgba(0,255,0,0.2)",
                                    borderColor: "rgba(0,255,0,1)",
                                    pointBorderColor: "#fff",
                                    pointBackgroundColor: "rgba(0,255,0,1)",
                                    pointBorderColor: "#fff",
                                    responsive: false,
                                    pointDot:false,
                                    showTooltips: false,
                                    scaleOverride: true,
                                    scaleSteps: 4,
                                    scaleStepWidth: 5,
                                    scaleStartValue: 0,
                                    data: [conce2, auto2, estres2, acti2, moti2, emocion2, obj2]
                                }
                            ]
                        },
                        options: {
                            responsive: false,
                            title: {
                                display: true,
                                text: 'Resultados de evaluación por Entrenamiento'
                            },
                            legend: {
                                display: false
                            }
                        }
                    });
                }else{
                    new Chart(document.getElementById("radars"), {
                        type: 'radar',
                        data: {
                            labels: ["ResistenciaFísica", "HábilidadMental", "CapacidadTécnica", "Talento"],
                            datasets: [
                                {
                                    label: "Habilidades",
                                    fill: true,
                                    backgroundColor: "rgba(32, 90, 249,0.2)",
                                    borderColor: "rgba(32, 90, 249, 1)",
                                    pointBorderColor: "#fff",
                                    pointBackgroundColor: "rgba(32, 90, 249,1)",
                                    pointBorderColor: "#fff",
                                    responsive: false,
                                    pointDot:false,
                                    showTooltips: false,
                                    scaleOverride: true,
                                    scaleSteps: 4,
                                    scaleStepWidth: 5,
                                    scaleStartValue: 0,
                                    data: [condi2, mental2, tecnica2, talento2]
                                }
    
                            ]
                        },
                        options: {
                            responsive: false,
                            title: {
                                display: true,
                                text: 'Resultados de evaluación por Hábilidades'
                            },
                            legend: {
                                display: false
                            }
                        }
                    });
                }
                
            } else {
                navigator.notification.alert(
                    data.msg,  // message
                    alertDismissed,         // callback
                    'Aviso',            // title
                    'Aceptar'                  // buttonName
                );
            }
        }
    });
    /**/
}