$(document).on('pagebeforeshow', '#page-variables', function (e) {
    var idU = localStorage.getItem('id_usuario');
    var t3 = "variables_usuario," + idU + "," + new Date().getTime();
    /**AJAX**/
    $.ajax({
        url: 'https://www.nerupsicologia.com/app/Php/data.php',
        type: 'post',
        data: { datos: t3 },
        dataType: 'json',
        success: function (data) {
            if (data.success) {
                $.each(data, function (index, record) {
                    if ($.isNumeric(index)) {
                        var checkB = "";
                        var chkVar = localStorage.getItem('check_variables');
                        var res1 = parseInt(record.resultado);
                        var res2 = parseInt(record.resultado2);
                        var resFinal;
                        var variable = record.id_variable;
                        if(variable != "8" && variable != "9" && variable != "10" && variable != "11"){
                            if (res1 < res2) {
                                resFinal = res1;
                            } else {
                                resFinal = res2;
                            }
                            if (resFinal < 8) {
                                if (record.status_trabajar == 1 && chkVar == "1") {
                                    checkB = "<input type='checkbox' name='varis' id=" + record.id_variable + " disabled checked class='singleCheck form-control'>";
                                    divA = "<div class='ui-bar ui-bar-a' id=" + record.id_variable + " onClick='getActividad(" + record.id_variable + ")' style='text-transform:uppercase;color:white !important;border-color:#fff !important;border-radius:10%;border:3px solid;'>";
                                } else {
                                    checkB = "<input type='checkbox' name='varis' id=" + record.id_variable + " class='singleCheck form-control'>";
                                    divA = "<div class='ui-bar ui-bar-a' id=" + record.id_variable + " style='text-transform:uppercase;color:white !important'>";
                                }
                                $(".div_variables").append("<div class='ui-grid-a' style='margin-bottom: 1rem;'>" +
                                    "<div class='ui-block-a'>" +
                                    divA+
                                    record.nombre + "</div>" +
                                    "</div>" +
                                    "<div class='ui-block-b' style='background: none;'>" +
                                    "<div class='ui-bar ui-bar-a' style='margin-left: 25%;'>" +
                                    checkB +
                                    "</div>" +
                                    "</div>" +
                                    "</div>");
                            }
                        }
                    }
                });
            } else {
                navigator.notification.alert(
                    data.msg,  // message
                    alertDismissedd,         // callback
                    'Aviso',            // title
                    'Aceptar'                  // buttonName
                );
            }
        }
    });
    /**FIN_AJAX**/

    setTimeout(function () {
        var chkVar = localStorage.getItem('check_variables');
        if (chkVar != "1") {
            navigator.notification.alert(
                "Recuerda solo puedes elegir entre 2 variables para trabajar",  // message
                alertDismissedd,         // callback
                'Aviso',            // title
                'Aceptar'                  // buttonName
            );
        }
        var limit = 3;
        $('input.singleCheck').on('change', function (evt) {
            if ($('input:checkbox:checked').length >= limit) {
                this.checked = false;
                $("#div_vari_sel").css("display", "none");
            }
            if ($('input:checkbox:checked').length <= 2) {
                $("#div_vari_sel").css("display", "block");
            } else {
                $("#div_vari_sel").css("display", "none");
            }
            if (chkVar == "1") {
                $("#div_vari_sel").css("display", "none");
            }
        });
    }, 1000);

});
function guardar_variables() {
    var listaCompras = '';
    $("input[name=varis]").each(function (index) {
        if ($(this).is(':checked')) {
            listaCompras += $(this).attr('id') + ',';
        }
    });
    localStorage.setItem('check_variables', '1');
    var idU = localStorage.getItem('id_usuario');
    var t3 = "actualiza_variables," + listaCompras + idU + "," + new Date().getTime();
    $.ajax({
        url: 'https://www.nerupsicologia.com/app/Php/data.php',
        type: 'post',
        data: { datos: t3 },
        dataType: 'json',
        success: function (data) {
            if (data.success) {
                regresarPEM();
            } else {
                navigator.notification.alert(
                    "Ocurrio algún error",  // message
                    alertDismissed,         // callback
                    'Aviso',            // title
                    'Aceptar'                  // buttonName
                );
            }
        }
    });
}

function getActividad(vc) {
    $.mobile.changePage('pages/page-pem_info_concentra.html', {});
    localStorage.setItem("variable_id", vc);
}

$(document).on('pagebeforeshow', '#page-pem_info', function (e) {
    var idVari = localStorage.getItem('variable_id');
    var t3 = "actividad_individual," + idVari + "," + new Date().getTime();
    /**AJAX**/
    $.ajax({
        url: 'https://www.nerupsicologia.com/app/Php/data.php',
        type: 'post',
        data: { datos: t3 },
        dataType: 'json',
        success: function (data) {
            console.log(data);
            if (data.success) {
                $.each(data, function (index, record) {
                    var numAct = parseInt(index) + 1;
                    if ($.isNumeric(index)) {
                        $(".divActividad").append("<div class='ui-grid-a' onClick='getInstruccion(" + record.id_instrucciones + ")' id='actividad_'>" +
                            "<div class='ui-block-a' style='margin-left: 5%;width: 25%;'>" +
                            "<div class='ui-bar ui-bar-a'>" +
                            "<div class='circle'>" + numAct + "</div>" +
                            "</div>" +
                            "</div>" +
                            "<div class='ui-block-b' style='background: none;width: 70%;'>" +
                            "<div class='ui-bar ui-bar-a'>" +
                            "<textarea readonly='true' placeholder='Cuadro de texto con información sobre las actividades' class='form-control'>" + record.descripcion + "</textarea>" +
                            "</div>" +
                            "</div>" +
                            "</div>");
                        $("#title_info_pem").text(record.nombre_variable);
                    }
                });
            } else {
                navigator.notification.alert(
                    "Sin Actividades",  // message
                    alertDismissed,         // callback
                    'Aviso',            // title
                    'Aceptar'                  // buttonName
                );
                setTimeout(function () {
                    $.mobile.changePage('pages/page-variables.html', {});
                }, 1000);
            }
        }
    });
    /**FIN_AJAX**/
});

function getInstruccion(id_act) {
    //$.mobile.changePage('pages/page-pem_actividad.html', {});
    $.mobile.changePage('pages/page-pem_actividad.html', {});
    localStorage.setItem("actividad_id", id_act);
}