document.addEventListener("backbutton", onBackKeyDown, false);
$("#btnRegistro").click( function(){
    $.mobile.changePage('pages/page-registro.html', {});
});
$("#btnOlvido").click( function(){
    $.mobile.changePage('pages/page-cambio-pass.html', {});
});  
function inicio(){
    $.mobile.changePage('pages/home.html', {});
}
function regresarPEM(){
    var pem = localStorage.getItem('var_pem');
    if(pem == 0){
        $.mobile.changePage('pages/page-programa_entrenamiento.html', {});
    }else{
        $.mobile.changePage('pages/page-variables.html', {});
    }
}

function page_grupos(x){
    cargarLoad();
    var status_pago = localStorage.getItem("pago");
    if (status_pago == "0") {
        cordova.plugin.pDialog.dismiss();
        if(x == 1){
            navigator.notification.alert(
                "Debes suscribirte para poder acceder a esta sección",  // message
                alertDismissedd,         // callback
                'Aviso',            // title
                'Aceptar'                  // buttonName
            );
        }else{
            setTimeout(function(){ $("#popupInfo").popup("open");}, 2000);
        }
    }else{
        cordova.plugin.pDialog.dismiss();
        $.mobile.changePage('pages/page-grupo.html', {});
    }    
}
function page_pagos(){
    cargarLoad();
    $.mobile.changePage('pages/page-pagos.html', {});
}
function perfil(){
    $.mobile.changePage('pages/page-perfil.html', {});
}
function page_rv(){
    var status_pago = localStorage.getItem("pago");
    /*if (status_pago == "0") {
        navigator.notification.alert(
            "Debes suscribirte para poder acceder a esta sección",  // message
            alertDismissedd,         // callback
            'Aviso',            // title
            'Aceptar'                  // buttonName
        );
    }else{*/
        $.mobile.changePage('pages/page-playsic_rv.html', {});
    //}
}
function rendimiento_general(){
    $.mobile.changePage('pages/page-rendimiento_general.html', {});
}
function cerrar_sesion(){
    navigator.notification.confirm(
        '¿Desas Cerrar Sesion de NERü?', // message
        onConfirm,            // callback to invoke with index of button pressed
        'Aviso',           // title
        ['Si','No']     // buttonLabels
    );

    function onConfirm(buttonIndex) {
        if (buttonIndex == 1) {
            localStorage.clear();
            $.mobile.changePage('presentacion.html', {});
        }
    }
    
}
function ayuda(){
    $.mobile.changePage('pages/page-ayuda.html', {});
}
function onBackKeyDown(){
    //alert($.mobile.activePage.attr("id"));
    if($.mobile.activePage.attr("id") == "page-home"){
        navigator.notification.confirm(
            '¿Desas salir de NERü?', // message
            onConfirm,            // callback to invoke with index of button pressed
            'Aviso',           // title
            ['Si','No']     // buttonLabels
        );
        function onConfirm(buttonIndex) {
            if (buttonIndex == 1) {
                navigator.app.exitApp();
                
            }
        }
    }
    var bb = localStorage.getItem("var_pem");
    var objG = localStorage.getItem('objetivoG');
    /*REGISTRO*/
    if($.mobile.activePage.attr("id") == "page-registro" || $.mobile.activePage.attr("id") == "page-cambiopass"){
        $.mobile.changePage('../presentacion.html', {});
    }
    /*FIN*/
    if($.mobile.activePage.attr("id") == "page-grupo"||$.mobile.activePage.attr("id") == "page-variables" || $.mobile.activePage.attr("id") == "page-pago" || $.mobile.activePage.attr("id") == "page-perfil" || $.mobile.activePage.attr("id") == "page-grupo_info" || $.mobile.activePage.attr("id") == "page-realidad_v" || $.mobile.activePage.attr("id") == "page-ayuda"){
        $.mobile.changePage('pages/home.html', {});
    }
    if($.mobile.activePage.attr("id") == "page-pem"){
        if(objG == "1"){
            navigator.notification.confirm(
                '¿Desas salir de NERü, aun no has terminado de tus evaluaciones?', // message
                onConfirm,            // callback to invoke with index of button pressed
                'Aviso',           // title
                ['Si','No']     // buttonLabels
            );
            function onConfirm(buttonIndex) {
                if (buttonIndex == 1) {
                    //navigator.app.exitApp();
                    cordova.plugins.notification.local.schedule({
                        title: 'Regresa pronto',
                        text: 'Revisa tus variables...',
                        foreground: true,
                        vibrate:true,
                        trigger: { every: 6, unit: 'hour' }
                    });
                    $.mobile.changePage('pages/home.html', {});
                }
            }
        }else{
            $.mobile.changePage('pages/home.html', {});
        }
    }
    if($.mobile.activePage.attr("id") == "page-variable" || $.mobile.activePage.attr("id") == "page-objetivos" || $.mobile.activePage.attr("id") == "page-all_variable"){
        $.mobile.changePage('pages/page-programa_entrenamiento.html', {});
    }
    if($.mobile.activePage.attr("id") == "page-evaluacion"){
        if(bb == "0"){
            $.mobile.changePage('pages/page-programa_entrenamiento.html', {});
        }else{
            $.mobile.changePage('pages/page-variables.html', {});    
        }
    }
    if($.mobile.activePage.attr("id") == "page-pem_info"){
        $.mobile.changePage('pages/page-variables.html', {});
    }
    if($.mobile.activePage.attr("id") == "page-pem_actividad" || $.mobile.activePage.attr("id") == "page-pem_actividad_detalle"){
        $.mobile.changePage('pages/page-pem_info_concentra.html', {});
    }
    if($.mobile.activePage.attr("id") == "page-pem_grafica"){
        $.mobile.changePage('pages/page-perfil_individual.html', {});
    }
    if($.mobile.activePage.attr("id") == "page-pem_grafica_ind"){
        $.mobile.changePage('pages/page-perfil.html', {});
    }
    if($.mobile.activePage.attr("id") == "page-perfil_individual" || $.mobile.activePage.attr("id") == "page-rendimiento_general"){
        $.mobile.changePage('pages/page-grupo_info.html', {});
    }
    if($.mobile.activePage.attr("id") == "page_info_rendimiento"){
        $.mobile.changePage('pages/page-pem_grafica_ind.html', {});
    }
    if($.mobile.activePage.attr("id") == "page_info_rendimientoD"){
        $.mobile.changePage('pages/page-rendimiento_general.html', {});
    }
    if($.mobile.activePage.attr("id") == "page_info_grafica"){
        var r = localStorage.getItem("graficaInd");
        if(r == '0'){
            $.mobile.changePage('pages/page-pem_grafica_ind.html', {});
        }else if(r == '4'){
            $.mobile.changePage('pages/page-rendimiento_general.html', {});
        }else{
            $.mobile.changePage('pages/page-pem_grafica.html', {});
        }
        
    }
}
