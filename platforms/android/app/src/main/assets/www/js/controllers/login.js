document.addEventListener("online", onOnline, false);
//cordova.plugin.pDialog.dismiss();
function onOnline() {
    var networkState = navigator.connection.type;
    
    var states = {};
    states[Connection.UNKNOWN]  = 'Unknown connection';
    states[Connection.ETHERNET] = 'Ethernet connection';
    states[Connection.WIFI]     = 'WiFi connection';
    states[Connection.CELL_2G]  = 'Cell 2G connection';
    states[Connection.CELL_3G]  = 'Cell 3G connection';
    states[Connection.CELL_4G]  = 'Cell 4G connection';
    states[Connection.CELL]     = 'Cell generic connection';
    states[Connection.NONE]     = 'No network connection';

    if(states[networkState] == "No network connection"){
        $.mobile.changePage('../presentacion.html', {});
    }
}
$(document).on('pagebeforeshow', '#page-login', function (e) {

    setTimeout(function(){ 
        cordova.plugin.pDialog.dismiss();
    }, 1000);
});
function alertNada() {
    $.mobile.changePage('presentacion.html', {});
}
function iniciar(){
    var usuario = $("#usuario").val();
    var pass = $("#passwords").val();
    var t3 = "iniciar_sesion,"+usuario+","+pass+","+new Date().getTime();
    cordova.plugin.pDialog.init({
        theme : 'HOLO_DARK',
        progressStyle : 'SPINNER',
        cancelable : false,
        title : 'Espere por favor...',
        message : 'Cargando ...'
    });
    if(pass.length == 0){
        navigator.notification.alert(
            "Falta Ingresar Contraseña",  // message
            alertNada,         // callback
            'Aviso',            // title
            'Aceptar'                  // buttonName
        );
        cordova.plugin.pDialog.dismiss();
        return;
    }
    /**AJAX**/
    $.ajax({
        url:'https://www.nerupsicologia.com/app/Php/data.php',
        type: 'post',
        data: {datos:t3},
        dataType: 'json',
        success: function(data){
            if(data.success){
                $.each(data, function(index, record){
                    if($.isNumeric(index)){
                        localStorage.setItem("activo", record.activo);
                        localStorage.setItem("var_pem", record.status_variable);
                        localStorage.setItem("id_usuario", record.id);
                        localStorage.setItem("pago", record.stado_pago);
                        localStorage.setItem("imgData", record.imagen);
                    }
                });
                $.mobile.changePage('pages/home.html', {});
            }else{
                navigator.notification.alert(
                    data.msg,  // message
                    alertNada,         // callback
                    'Aviso',            // title
                    'Aceptar'                  // buttonName
                );
                $("#passwords").val("");
                cordova.plugin.pDialog.dismiss();
                return;
            }
            $("#usuario").val("");
            $("#passwords").val("");
        }
    });
    /**FIN_AJAX**/        
}