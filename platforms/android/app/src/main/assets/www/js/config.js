var Config = new function ()
{
    this.pathModels = "www/js/models/";
    this.pathControllers = "www/js/controllers/";
    //produccion
    //this.urlNodeJS = "http://67.205.96.42";
    //this.port = "2085";
    //local
    //this.urlNodeJS = "http://192.168.3.51";
    //this.port = "8081";
    this.urlNodeJS = "http://192.168.1.122";
    this.port = "8080";
    this.controllers = new Array();
    this.internetSevices = true;
    this.busySincronize = false;
    this.busySincronize2 = false;
    this.neru = "PRUEBA";
    


    this.checkConfig = function () {
        var perfil = Config.getPerfil();

        if (perfil == null)
        {            //proceso de configuracion
            $.mobile.changePage('page-perfil.html', {});
        } else {
            //proceso de configuracion
            if (perfil.idPerfil == 1) {
                ConfigPerfilCtrl.CargaCssJs(perfil);

            } else {
                if (perfil.idPerfil == 2) {
                    ConfigPerfilCtrl.CargaCssJs(perfil);
                }
            }
        }
    }


    this.getPerfil = function () {
        if (typeof (Storage) !== "undefined")
        {
            var value = localStorage.getItem("perfil");
            if (!value) {
                return null;
            }
            if (value[0] === "{") {
                value = JSON.parse(value);
            }
            return value;
        }
    };

    this.setPerfil = function (perfil) {
        if (typeof (Storage) !== "undefined")
        {
            localStorage.setItem("perfil", JSON.stringify(perfil));
        }
    };
}