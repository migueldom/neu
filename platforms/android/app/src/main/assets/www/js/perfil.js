//document.addEventListener("deviceready", onDeviceReady, false);
document.addEventListener("offline", onOffline, false);
function alertExit() {
    navigator.app.exitApp();
}
function onOffline() {
    navigator.notification.alert(
        "Sin Internet, verifica tu conexión",  // message
        alertExit,         // callback
        'Aviso',            // title
        'Aceptar'                  // buttonName
    );
    
}
var sesion = localStorage.getItem('activo');

if(sesion == 1 || sesion == "1"){
    
    setTimeout(function(){ 
        $.mobile.changePage('pages/home.html', {}); },
    1000);
}else{
    localStorage.clear();
}
