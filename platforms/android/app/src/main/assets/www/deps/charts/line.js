document.addEventListener("deviceready", function () {

    viewPortAction.add(document.getElementById("line-chart"), function () {

        am4core.useTheme(am4themes_animated);
        
        var chart = am4core.create("line-chart", am4charts.XYChart);
        chart.hiddenState.properties.opacity = 0; // this makes initial fade in effect
        
        chart.paddingRight = 20;
        
        var data = [];
        var visits = 10;
        for (var i = 1; i < 30; i++) {
            visits += Math.round((Math.random() < 0.5 ? 1 : -1) * Math.random() * 10);
            data.push({ date: new Date(2018, 0, i), value: visits });
        }
        
        chart.data = data;
        
        var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
        dateAxis.renderer.grid.template.location = 0;
        dateAxis.renderer.axisFills.template.disabled = true;
        dateAxis.renderer.ticks.template.disabled = true;
        dateAxis.renderer.labels.template.rotation = 270;
        dateAxis.renderer.labels.template.hideOversized = false;
        dateAxis.renderer.labels.template.horizontalCenter = "right";
        dateAxis.renderer.labels.template.verticalCenter = "middle";
        
        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.tooltip.disabled = true;
        valueAxis.renderer.minWidth = 35;
        valueAxis.renderer.axisFills.template.disabled = true;
        valueAxis.renderer.ticks.template.disabled = true;
        
        var series = chart.series.push(new am4charts.LineSeries());
        series.dataFields.dateX = "date";
        series.dataFields.valueY = "value";
    
    });
});